﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace CommSpace
{
    class Comm
    {
        public delegate void EventHandle(string port,byte[] readBuffer);
        public event EventHandle DataReceivedHandle;

        public delegate void ErrorHandle(string port, string mesage);
        public event ErrorHandle CommErrorHandle;

        private SerialPort serialPort;
        private Thread thread;
        private volatile bool _keepReading;
        
        /// <summary>
        /// 构造函数
        /// </summary>
        public Comm()
        {   
            serialPort = new SerialPort();
            thread = null;
            _keepReading = false;
        }

       ~Comm()
        {
            Close();
        }

        /// <summary>
        /// 串口是否打开
        /// </summary>
        /// <returns></returns>
        public bool isOpen()
        {
             return serialPort.IsOpen;
        }

        /// <summary>
        /// 开始监听
        /// </summary>
        /// <returns></returns>
        private void StartReading()
        {
            if (!_keepReading)
            {
                _keepReading = true;
                thread = new Thread(new ThreadStart(ReadPort));
                thread.IsBackground = true;
                thread.Start();
            }
        }

        private void StopReading()
        {
            if (_keepReading)
            {
                _keepReading = false;
                thread.Join();
                thread = null;
            }
        }

        /// <summary>
        /// 读取数据
        /// </summary>
        private void ReadPort()
        {
            while (_keepReading)
            {
                if (serialPort.IsOpen)
                {
                    int count = serialPort.BytesToRead;
                    if (count > 0)
                    {
                        byte[] readBuffer = new byte[count];
                        try
                        {
                            //System.Windows.Forms.Application.DoEvents();
                            serialPort.Read(readBuffer, 0, count);
                            if (null != DataReceivedHandle)
                            {
                                DataReceivedHandle(serialPort.PortName, readBuffer);
                            }
                            Thread.Sleep(100);
                        }
                        catch (TimeoutException e)
                        {
                            if (null != CommErrorHandle)
                            {
                                CommErrorHandle(serialPort.PortName, e.Message);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 打开
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        /// <param name="dataBits"></param>
        /// <param name="stopBits"></param>
        /// <param name="parity"></param>
        /// <param name="readTimeout"></param>
        /// <param name="writeTimeout"></param>
        /// <returns></returns>
        public bool Open(string portName,int baudRate, int dataBits,int stopBits, int parity, int readTimeout, int writeTimeout)
        {
            serialPort.BaudRate = baudRate;
            serialPort.DataBits = dataBits;
            serialPort.StopBits = (StopBits)stopBits;
            serialPort.Parity = (Parity)parity;
            serialPort.ReadTimeout = readTimeout;
            serialPort.WriteTimeout = writeTimeout;
            serialPort.PortName = portName;
 
            try
            {
                Close();
                serialPort.Open();
            }
            catch (System.Exception ex)
            {
                if (null != CommErrorHandle)
                {
                    CommErrorHandle(serialPort.PortName, ex.Message);
                }
            }
            if (serialPort.IsOpen)
                StartReading();

            return serialPort.IsOpen;
        }

        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
        {
            StopReading();
            serialPort.Close();
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="offset">偏移量</param>
        /// <param name="count">数据长度</param>
        public void WriteData(byte[] data, int offset, int count)
        {
            if (isOpen())
            {
                serialPort.Write(data, offset, count);
            }
        }

    }
}
