﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommSpace;

namespace CommDemo
{
    class Program
    {

        static Comm comm = new Comm();
        static void Main(string[] args)
        {
            comm.DataReceivedHandle += new Comm.EventHandle(comm_DataReceivedHandle);
            comm.CommErrorHandle += new Comm.ErrorHandle(comm_CommErrorHandle);
            if (false == comm.Open("COM3",9600,8,1,0,1000,-1))
            {
                Console.WriteLine("打开串口失败");
            }
            else
            {
                Console.WriteLine("打开串口成功");
            }

            Console.Read();
        }

        static void comm_CommErrorHandle(string port, string mesage)
        {
            Console.WriteLine(port + " error:" + mesage);
        }

        static void comm_DataReceivedHandle(string port, byte[] readBuffer)
        {
            string str = System.Text.Encoding.Default.GetString(readBuffer);
            Console.WriteLine(port + " recv: " + str);

            str = "OK, I got it!\n";
            byte[] data = System.Text.Encoding.Default.GetBytes(str);
            comm.WriteData(data, 0, data.Length);
        }
    }
}
