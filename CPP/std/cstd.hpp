#ifndef HEADER_CSTD_SHIQINGLIANG__
#define HEADER_CSTD_SHIQINGLIANG__

#include <libs_version.h>
#include <list>
#include <CSynchronization.hpp>
#include <map>
#include <vector>

using namespace std;

#pragma region LIST

template<typename T>
class CMyList: public std::list<T>
{
public:
	CMyList(){};
	~CMyList(){};

	CSynchroLock mSynchronLock;

	void Lock(){mSynchronLock.work();};
	void Unlock(){mSynchronLock.unwork();};


	void AddTail(T node)
	{
		mSynchronLock.do_lock();
		__super::push_back(node);
		mSynchronLock.do_unlock();
	}

	void AddHead(T node)
	{
		mSynchronLock.do_lock();
		__super::push_front(node);
		mSynchronLock.do_unlock();
	}

	char GetTail(T& vct, char remove = 1)
	{
		list<T>::iterator iter;
		char res = 0;
		mSynchronLock.do_lock();
		if (false == __super::empty())
		{
			iter = end();
			iter--;
			vct = *iter;
			if(1 == remove) pop_back();
			res = 1;
		}
		mSynchronLock.do_unlock();
		return res;
	}

	char GetHead(T& vct, char remove = 1)
	{
		char res = 0;
		list<T>::iterator iter;
		mSynchronLock.do_lock();
		if (false == m_list.empty())
		{
			iter = m_list.begin();
			vct = *iter;
			if (remove)	m_list.pop_front();
			res = 1;
		}
		mSynchronLock.do_unlock();

		return res;
	}
};
#pragma endregion LIST

#pragma region MAP

template<typename T1, typename T2>
class CMyMap: public map<T1,T2>
{
public:

	CMyMap(){};
	~CMyMap(){};

private:
	CSynchroLock mSynchronLock;

public:

	void mInsert(T1 key, T2 value, bool replace = true)
	{
		mSynchronLock.work();
		iterator iter = find(key);
		if(iter == __super::end())
		{
			__super::insert(pair<T1,T2>(key,value));
		}
		else
		{
			if(true == replace)	iter->second = value;
		}
		mSynchronLock.unwork();
	}

	void mRemove(T1 key)
	{
		mSynchronLock.work();
		iterator iter = __super::find(key);
		if(iter != __super::end())
			__super::erase(iter);
		mSynchronLock.unwork();
	}

	char mFind(T1 key, T2& v)
	{
		char res = 0;
		mSynchronLock.work();
		iterator iter = __super::find(key);
		if(iter != __super::end()){
			res  = 1;		
			v = iter->second;
		}
		mSynchronLock.unwork();
		return res;
	}

	char mFind(T1 key,T2** v)
	{
		char res = 0;
		mSynchronLock.work();
		iterator iter = __super::find(key);
		if(iter != __super::end()){
			res  = 1;		
			*v = &(iter->second);
		}
		mSynchronLock.unwork();
		return res;
	}

	char mIsExit(T1 key)
	{
		char res = 0;
		mSynchronLock.work();
		iterator iter = __super::find(key);
		res = (iter == __super::end()) ? 0:1;
		mSynchronLock.unwork();
		return res;
	}

	void Lock(){mSynchronLock.work();};
	void Unlock(){mSynchronLock.unwork();};

};
#pragma endregion MAP


#pragma region VECTOR

template<typename T>
class CMyVector: public std::vector<T>
{
public:
	CMyVector(){};
	~CMyVector(){};

	CSynchroLock mSynchronLock;

	void Lock(){mSynchronLock.work();};
	void Unlock(){mSynchronLock.unwork();};

	void PushBack(T node)
	{
		Lock();
		__super::push_back(node);
		Unlock();
	}

	void PopBack()
	{
		Lock();
		__super::pop_back();
		Unlock();
	}

};


#pragma endregion VECTOR

#endif