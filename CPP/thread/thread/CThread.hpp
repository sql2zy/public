#ifndef __HEADER_CTHREAD_SHIQINGLIANG__
#define __HEADER_CTHREAD_SHIQINGLIANG__

namespace sql_thread
{
#include <Windows.h>
#include <process.h>

#ifndef _BIND_TO_CURRENT_VCLIBS_VERSION
#define _BIND_TO_CURRENT_VCLIBS_VERSION 1
#endif

	class CThread
	{
	public:
		CThread(void)
		{
			stt = INVALID_HANDLE_VALUE;
			is_runing = is_stop = false;
			th_id = 0;
		}
		~CThread(void){join();};

	private:
		HANDLE stt;
		volatile bool  is_runing;
		volatile bool is_stop;
		unsigned int th_id;

	public:
		
		/***
		 * 名  称: start
		 * 功  能: 启动线程
		 * 作  者: Shi Qingliang 
		 * 参  数: unsigned 
		 * 参  数: __stdcall * _startAddress
		 * 参  数: 
		 * 参  数: void * 
		 * 参  数: LPVOID Arglist
		 * 参  数: void * security
		 * 参  数: unsigned int _StackSize
		 * 参  数: unsigned int _InitFlag
		 * 参  数: int Priority
		 * 日  期: 2017/03/17
		 * 返  回: bool
		 */
		bool start(unsigned (__stdcall * _startAddress) (void *),LPVOID Arglist = NULL,void*security = NULL, 
			unsigned int _StackSize = 0,unsigned int _InitFlag = 0, int Priority = 0)
		{
			if (false == is_runing)
			{
				stt = (HANDLE)_beginthreadex(security, _StackSize, _startAddress, Arglist, 0, &th_id);
				if (!stt)return false;

				if (!SetThreadPriority(stt, Priority))return false;
				is_runing = true;
				std::cout<<"启动线程:"<<stt<<std::endl;

			}
			return is_runing;
		}


		
		/***
		 * 名  称: join
		 * 功  能: 结束
		 * 作  者: Shi Qingliang 
		 * 日  期: 2017/03/17
		 * 返  回: void
		 */
		void join()
		{
			if (stt != INVALID_HANDLE_VALUE)
			{
				std::cout<<"结束线程:"<<stt<<std::endl;
				is_stop = true;
				WaitForSingleObject(stt,/*INFINITE*/1000);
				CloseHandle(stt);
				stt = INVALID_HANDLE_VALUE;
				is_runing = false;
			}
		}

		
		/***
		 * 名  称: is_continue
		 * 功  能: 是否继续
		 * 作  者: Shi Qingliang 
		 * 日  期: 2017/03/17
		 * 返  回: bool
		 */
		bool is_continue(){return !is_stop;};

		
		/***
		 * 名  称: check_status
		 * 功  能: 查看是否运行
		 * 作  者: Shi Qingliang 
		 * 日  期: 2017/03/17
		 * 返  回: bool
		 */
		bool check_status(){return is_runing;};
	};

}




#endif