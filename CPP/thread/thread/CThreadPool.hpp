#ifndef _HEADER_CTHREADPOOL_SHIQINGLIANG__
#define _HEADER_CTHREADPOOL_SHIQINGLIANG__


#ifndef _BIND_TO_CURRENT_VCLIBS_VERSION
	#define  _BIND_TO_CURRENT_VCLIBS_VERSION 1
#endif

#include <Windows.h>
#include <iostream>
#include <queue>

#include <CSynchronization.hpp>
#include "CThread.hpp"

namespace sql_thread
{
	template<typename T>
	class CThreadPool
	{
	public:
		/***
		 * 名  称: CThreadPool
		 * 功  能: 构造函数
		 * 作  者: Shi Qingliang 
		 * 参  数: void
		 * 日  期: 2017/03/17
		 * 返  回: 
		 */
		CThreadPool(void){
			is_runging		= true;
			mThreadSize		= 0;
			mWorkingSize	= 0;
			mActivieSize	= 0;
		};
		
		/***
		 * 名  称: ~CThreadPool
		 * 功  能: 析构函数
		 * 作  者: Shi Qingliang 
		 * 参  数: void
		 * 日  期: 2017/03/17
		 * 返  回: 
		 */
		~CThreadPool(void){join();};
		volatile unsigned int mThreadSize;
		volatile unsigned int mActivieSize;
		volatile unsigned int mWorkingSize;

		typedef void(*CTP_FUNCTION)(T&);

	private:

		template<typename X>
		class CTask{
		public:
			CTask(CTP_FUNCTION f,X lpvoid):mFuntion(f),mlPvoid(lpvoid){};
			CTask(void){};
			~CTask(void){};
			
			CTP_FUNCTION mFuntion;
			X mlPvoid;
			
			CTask& operator = (const CTask& f)
			{
				mFuntion = f.mFuntion;
				mlPvoid = f.mlPvoid;
				return *this;
			}
	
		};

		std::queue<CTask<T>> mQueueTask;
		std::queue<CThread*> mThreadQueue;
		CSynchroEvent mTQEvent;
		CSynchroLock mTQLock, mPoolLock;
		bool is_runging;

		
		/***
		 * 名  称: getTask
		 * 功  能: 获取任务
		 * 作  者: Shi Qingliang 
		 * 参  数: CTask & task
		 * 日  期: 2017/03/20
		 * 返  回: bool
		 */
		bool getTask(CTask<T>& task)
		{
			bool ret = false;
			mPoolLock.do_lock();
			mTQEvent.WaitForEvent();
			mPoolLock.do_unlock();
			mTQEvent.ResetSynchroEvent();
			mTQLock.do_lock();
			printf("Queue size:%d\n",mQueueTask.size());
			if (mQueueTask.size() > 0)
			{
				task = mQueueTask.front();
				mQueueTask.pop();
				ret = true;
			}
			mTQLock.do_unlock();

			if (mQueueTask.size() > 0){
				printf("SetSynchroEvent ---> getTask\n");
				mTQEvent.SetSynchroEvent();
			}
			return ret;
		}

		/***
		 * 名  称: tp_rooter
		 * 功  能: 线程接口
		 * 作  者: Shi Qingliang 
		 * 参  数: void * lpvoid
		 * 日  期: 2017/03/20
		 * 返  回: unsigned WINAPI
		 */
		static unsigned WINAPI tp_rooter(LPVOID lpvoid)
		{
			if(lpvoid)
			{
				CThreadPool* tp = static_cast<CThreadPool*>(lpvoid);
				tp->_tp_router_();
			}
			return 0;
		}

		
		/***
		 * 名  称: _tp_router_
		 * 功  能: 主线程
		 * 作  者: Shi Qingliang 
		 * 参  数: void * lpvoid
		 * 日  期: 2017/03/20
		 * 返  回: unsigned
		 */
		unsigned _tp_router_()
		{
			static int i = 0;
			CTask<T> task;
			mActivieSize++;
			printf("_tp_router_ : %d\n",mActivieSize);
			while (true == is_runging)
			{
				if (getTask(task))
				{
					mWorkingSize++;
					if (task.mFuntion)
						task.mFuntion(task.mlPvoid);
					mWorkingSize--;
				}
				else{
					printf("<><><><><><><><><>\n");
					//break;
					Sleep(10);
				}
			}
			mActivieSize--;
			//printf("_tp_router_ -----------------: %d\n",mActivieSize);
			return 1;
		}

	public:

		/***
		 * 名  称: addTask
		 * 功  能: 添加任务
		 * 作  者: Shi Qingliang 
		 * 参  数: CTP_FUNCTION f
		 * 参  数: LPVOID lpvoid
		 * 日  期: 2017/03/17
		 * 返  回: bool
		 */
		bool addTask(CTP_FUNCTION f,T lpvoid)
		{
			bool ret = false;
			if ((mQueueTask.size() < mThreadSize) && true == is_runging)
			{
				std::cout<<"QueueSize:"<<mQueueTask.size()<<" Act:"<<mActivieSize<<"  working:"<<mWorkingSize<<std::endl;
				CTask<T> task(f,lpvoid);
				mTQLock.do_lock();
				mQueueTask.push(task);
				mTQLock.do_unlock();
				printf("SetSynchroEvent ---> addTask\n");
				mTQEvent.SetSynchroEvent();
				ret = true;
			}
			return ret;
		}

		
	
	/***
	 * 名  称: initThreadPool
	 * 功  能: 初始化
	 * 作  者: Shi Qingliang 
	 * 参  数: unsigned int size
	 * 日  期: 2017/03/20
	 * 返  回: bool
	 */
	bool initThreadPool(unsigned int size)
	{
		mThreadSize = size;
		for (unsigned int i=0; i<mThreadSize; i++)
		{
			CThread* thread = new CThread();
			if (NULL == thread)
				thread = new CThread();

			if (thread)
			{
				mThreadQueue.push(thread);
				thread->start(tp_rooter,this);
			}
		}
		return true;
	}
		/***
		 * 名  称: join
		 * 功  能: 结束线程池
		 * 作  者: Shi Qingliang 
		 * 日  期: 2017/03/20
		 * 返  回: void
		 */
		void join()
		{
			sql_thread::CThread *thread = NULL;
			is_runging = false;
			while (mQueueTask.size() > 0)
				mQueueTask.pop();

			while(mActivieSize > 0){
				printf("SetSynchroEvent ---> join\n");
				mTQEvent.SetSynchroEvent();
			}

			unsigned int count = mThreadQueue.size();
			for (unsigned int i=0; i< count; i++)
			{
				thread = mThreadQueue.front();
				thread->join();
				delete thread;
				mThreadQueue.pop();
			}
		}
	};
} 

#endif //_HEADER_CTHREADPOOL_SHIQINGLIANG__