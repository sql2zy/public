#ifndef _HEADER_CTHREADPOOL_SHIQINGLIANG__
#define _HEADER_CTHREADPOOL_SHIQINGLIANG__


#ifndef _BIND_TO_CURRENT_VCLIBS_VERSION
	#define  _BIND_TO_CURRENT_VCLIBS_VERSION 1
#endif

#include <Windows.h>
#include <iostream>
#include <queue>

#include <CSynchronization.hpp>


typedef void(*CTP_FUNCTION)(void);

namespace sql_thread
{
	class CThreadPool
	{
	public:
		/***
		 * 名  称: CThreadPool
		 * 功  能: 构造函数
		 * 作  者: Shi Qingliang 
		 * 参  数: void
		 * 日  期: 2017/03/17
		 * 返  回: 
		 */
		CThreadPool(void){};
		
		/***
		 * 名  称: ~CThreadPool
		 * 功  能: 析构函数
		 * 作  者: Shi Qingliang 
		 * 参  数: void
		 * 日  期: 2017/03/17
		 * 返  回: 
		 */
		~CThreadPool(void){};

		unsigned int mThreadSize;
		unsigned int mActivieSize;
		HANDLE* mWorkThreadIDS;

	private:

		class CTask{
		public:
			CTask(CTP_FUNCTION f,LPVOID lpvoid):mFuntion(f),mlPvoid(lpvoid){};
			~CTask(void){};
			
			CTP_FUNCTION mFuntion;
			LPVOID mlPvoid;
			
			CTask& operator = (const CTask& f)
			{
				mFuntion = f.mFuntion;
				mlPvoid = f.mlPvoid;
				return *this;
			}
		};

		std::queue<CTask> mQueueTask;
		std::queue<sql_thread::CThread*> mThreadQueue;
		CSynchroEvent mTQEvent;
		CSynchroLock mTQLock;

		
		/***
		 * 名  称: addTask
		 * 功  能: 添加任务
		 * 作  者: Shi Qingliang 
		 * 参  数: CTP_FUNCTION f
		 * 参  数: LPVOID lpvoid
		 * 日  期: 2017/03/17
		 * 返  回: bool
		 */
		bool addTask(CTP_FUNCTION f,LPVOID lpvoid)
		{
			bool ret = false;
			if (mQueueTask.size() < max_size)
			{
				CTask task(f,lpvoid);
				mTQLock.do_lock();
				mQueueTask.push(task);
				mTQLock.do_unlock();
				mTQEvent.SetSynchroEvent();
				ret = true;
			}
			return ret;
		}

		bool getTask(CTask& task)
		{
			bool ret = false;
			mTQEvent.WaitForEvent();
			mTQEvent.ResetSynchroEvent();
			mTQLock.do_lock();
			if (mQueueTask.size() > 0)
			{
				task = mQueueTask.front();
				mQueueTask.pop();
				ret = true;
			}
			mTQLock.do_unlock();

			return ret;
		}



	bool initThreadPool(unsigned int size)
	{
		DWORD dwRunThread;
		mThreadSize = size;

		mWorkThreadIDS = new HANDLE[mThreadSize];


		for (unsigned int i=0; i<mThreadSize; i++)
		{
			sql_thread::CThread* thread = new sql_thread::CThread();
			if (NULL == thread)
			{
				thread = new sql_thread::CThread();
			}
			if (thread)
			{
				mThreadQueue.push(thread);
				
			}
		}

	}


} 

#endif //_HEADER_CTHREADPOOL_SHIQINGLIANG__