#ifndef _HEADER_CTHREADPOOLUTIL_SHIQINGLIANG__
#define _HEADER_CTHREADPOOLUTIL_SHIQINGLIANG__

#include <iostream>
#include <CSynchronization.hpp>
#include <queue>

class CTaskQueue
{
public:
	CTaskQueue(void){};
	~CTaskQueue(void){};

private:

	std::queue<int> mQueue;
	CSynchroEvent mTaskEvent;
	CSynchroLock mTaskLock;
};

#endif //_HEADER_CTHREADPOOLUTIL_SHIQINGLIANG__