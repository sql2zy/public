#ifndef HEADER_TRACE_SHIQINGLIANG__
#define HEADER_TRACE_SHIQINGLIANG__
#include <Windows.h>
#include <ctype.h>


void _trace(char* format,...);

void _traceTime(char* format,...);

void _getlocaltimeStr(char* str, unsigned int len);

void _getstring(char* str, unsigned int len,char* format,...);

//show error
void _printf_err(DWORD err);
void _printf_err();

#endif