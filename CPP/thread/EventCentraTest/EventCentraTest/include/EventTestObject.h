#ifndef HEADER_EVENTTESTOBJECT_SHIQINGLIANG__
#define HEADER_EVENTTESTOBJECT_SHIQINGLIANG__

#include <CEventCentra.h>

class CEventObject: public CEventInterface
{
public:
	CEventObject(string name, int timeout);
	~CEventObject(void);

	string mName;
	int mTimeout;

	void do_work(unsigned int EventID, WPARAM wparam, LPARAM lparam, LPVOID data);

};

#endif //HEADER_EVENTTESTOBJECT_SHIQINGLIANG__