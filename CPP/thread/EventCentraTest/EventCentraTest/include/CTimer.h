#ifndef HEADER_CTIMER_SHIQINGLIANG__
#define HEADER_CTIMER_SHIQINGLIANG__
#include <Windows.h>
#include <vector>
#include <CSynchronization.hpp>
#include <wtypes.h>

typedef void(*TIMER_CALLBACK)(unsigned int,DWORD,DWORD,LPVOID );

//////////////////////////////////////////////////////////////////////////
//interface
class CTimerInterface
{
public:
	CTimerInterface(void){};
	~CTimerInterface(void){};

	virtual void Timer(unsigned int TimerID, DWORD wparam = 0, DWORD lparam = 0, LPVOID lpVoid = NULL){};
};

//////////////////////////////////////////////////////////////////////////
//
class CTimerNode
{
public:
	/***
	 * @brief: structrue & destructrue
	 */	
	CTimerNode(void);
	CTimerNode(CTimerInterface* cti,unsigned int id,DWORD dwMilliSeconds, DWORD wparam = 0, DWORD lparam = 0, LPVOID lpvoid = NULL);
	CTimerNode(TIMER_CALLBACK cb,unsigned int id,DWORD dwMilliSeconds, DWORD wparam = 0, DWORD lparam = 0, LPVOID lpvoid = NULL);
	~CTimerNode(void);

	/***
	 * @brief: clear
	 */
	void Zero();

	/***
	 * @brief: fill param
	 */
	void Fill(CTimerInterface* cti,unsigned int id,DWORD dwMilliSeconds, DWORD wparam = 0, DWORD lparam = 0, LPVOID lpvoid = NULL);
	void Fill(TIMER_CALLBACK cb,unsigned int id,DWORD dwMilliSeconds, DWORD wparam = 0, DWORD lparam = 0, LPVOID lpvoid = NULL);

	/***
	 * @brief: call function
	 */
	bool Call();

	CTimerNode& operator = (const CTimerNode& from);
	bool operator == (const CTimerNode& node) const;

	CTimerInterface* mInterface;
	TIMER_CALLBACK mCallback;
	unsigned int mID;
	DWORD wParam, lParam;
	LPVOID lpVoid;

	DWORD mdwMilliSeconds;
	SYSTEMTIME mStart;

protected:
	char mType;
};

//////////////////////////////////////////////////////////////////////////
//
class CTimer
{
public:
	/***
	 * @brief: structure & destructure
	 */
	CTimer(void);
	~CTimer(void);

	/***
	 * @brief: Add Timer
	 */
	bool SetTimer(CTimerInterface* cti,DWORD dwMilliSeconds, unsigned int TimerID = 0, DWORD wparam = 0, DWORD lparam = 0, LPVOID lpVoid = 0);
	bool SetTimer(TIMER_CALLBACK tb,DWORD dwMilliSeconds, unsigned int TimerID = 0, DWORD wparam = 0, DWORD lparam = 0, LPVOID lpVoid = 0);

	/***
	 * @brief: remove Timer
	 */
	bool KillTimer(CTimerInterface* cti, unsigned int TimerID);
	bool KillTimer(TIMER_CALLBACK tb, unsigned int TimerID);

private:

	/***
	 * @brief: 
	 */
	void router();

	/***
	 * @brief: add timer
	 */
	bool SetTimer(CTimerNode* tn);

	/***
	 * @brief: remove timer
	 */
	bool KillTimer(CTimerNode* tn);

	/***
	 * @brief:
	 */
	static unsigned WINAPI CTimer_Router(void* lpvoid);

	/***
	 * @brief: check router is runing
	 */
	void CheckRouter();

	bool mExit;							//flag to quit
	CSynchroLock mTimerLock;			//lock 
	HANDLE mTimerHandle;				// handle
	std::vector<CTimerNode> mVect;		//vector
};

#endif //HEADER_CTIMER_SHIQINGLIANG__