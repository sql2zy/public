// EventCentraTest.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <CEventCentra.h>
#include <EventTestObject.h>

#include <boost/thread/thread.hpp>

#include <iostream>  
#include <boost/thread.hpp>  
#include <stdlib.h>
#include <tuple>
#include "threadpool/CThread.hpp"
#include "threadpool/CThreadPool.hpp"

using namespace std;  
using namespace boost;  

CEventObject eo1("甲",1000);
CEventObject eo2("乙",1500);

//
void Task()
{
	while(1)
	{
		CEventCentra::GetInstance()->ec_Event(1);
		Sleep(500);
	}

}

unsigned int WINAPI router(void* p)
{
	sql_thread::CThread* t = (sql_thread::CThread*)p;
	while(t->is_continue())
	{
		CEventCentra::GetInstance()->ec_Event(1);
		Sleep(500);
	}
	printf("router is over\n");
	return 1;
}

class TTT
{
public:
	TTT(string name, int age):mName(name),mAge(age){};
	~TTT(void){};

	string mName;
	
	void show()
	{
		cout<<"name:"<<mName<<" age:"<<mAge<<endl;
	}

	class Inter{
	public:
		Inter(void){};
		~Inter(void){};

		void IShow()
		{
			TTT* p = (TTT*)((char*)this - offsetof(TTT,i));
			cout<<"Inter "<<p->mAge<<endl;
		}
	} i;
	int mAge;

};

int _tmain(int argc, _TCHAR* argv[])
{
	/*
	CEventCentra::GetInstance()->ec_Add("eo1",&eo1);
	CEventCentra::GetInstance()->ec_Add("eo2",&eo2);

	sql_thread::CThread thr;
	thr.start(router,&thr);

	getchar();
	thr.join();
	*/

	CThreadPool c;
	getchar();

	printf("program is over\n");
	return 0;
}

