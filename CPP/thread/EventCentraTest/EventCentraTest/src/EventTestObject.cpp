#include <libs_version.h>
#include <EventTestObject.h>
#include <iostream>

//
CEventObject::CEventObject(string name, int timeout)
{
	mName = name;
	mTimeout = timeout;
}

CEventObject::~CEventObject(void)
{
 
}

void CEventObject::do_work(unsigned int EventID, WPARAM wparam, LPARAM lparam, LPVOID data)
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	printf("----------> %-10s task start %02d:%02d:%02d:%03d\n",mName.c_str(),st.wHour,st.wMinute,st.wSecond,st.wMilliseconds);
	Sleep(mTimeout);
	GetLocalTime(&st);
	printf("----------> %-10s task end   %02d:%02d:%02d:%03d\n",mName.c_str(),st.wHour,st.wMinute,st.wSecond,st.wMilliseconds);
	//boost::thread t;
}