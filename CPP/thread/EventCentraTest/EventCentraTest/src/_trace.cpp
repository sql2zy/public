#include <libs_version.h>
#include <_trace.h>
#include <assert.h>


//
void _trace(char* format,...)
{
	va_list vl;
	char msg[1024] = {0};
	if(!format) return ;

	va_start(vl,format);
	vsprintf_s(msg,format,vl);
	va_end(vl);
	OutputDebugString(msg);
}

//
void _traceTime(char* format,...)
{
	char timestr[100] = {0};
	va_list vl;
	char msg[1024] = {0};
	if(!format) return ;

	_getlocaltimeStr(timestr,sizeof(timestr));
	va_start(vl,format);
	vsprintf_s(msg,format,vl);
	va_end(vl);

	_trace("[%s] %s",timestr,msg);

}

void _getlocaltimeStr(char* str, unsigned int len)
{
	assert(str != NULL);
	SYSTEMTIME st;
	GetLocalTime(&st);

	sprintf_s(str,len,"%04d-%02d-%02d %02d:%02d:%02d:%03d",
		st.wYear,st.wMonth,st.wDay,st.wHour,st.wHour,st.wSecond,st.wMilliseconds);

}

void _getstring(char* str, unsigned int len,char* format,...)
{
	assert(str != NULL);
	va_list vl;
	char msg[1024] = {0};

	if(format)
	{
		va_start(vl,format);
		vsprintf_s(msg,format,vl);
		va_end(vl);
	}
}

void _printf_err(DWORD err)
{
	LPSTR lpBuffer;		
	LPVOID lpstr = NULL;
	FormatMessage ( FORMAT_MESSAGE_ALLOCATE_BUFFER  |	
		FORMAT_MESSAGE_IGNORE_INSERTS  |			
		FORMAT_MESSAGE_FROM_SYSTEM,				
		NULL,									
		err,										
		LANG_NEUTRAL,							
		(LPTSTR) & lpBuffer,					
		0 ,										
		NULL );									
	lpstr  = lpBuffer ? (lpBuffer) : " Sorry, cannot find this error info.";

	_trace((char*)lpstr);
	LocalFree (lpBuffer);
}
void _printf_err()
{
	_printf_err(GetLastError());
}

