#include <libs_version.h>

#include <BaseNet.h>



// 构造函数
CBaseSocket::CBaseSocket(void)
{
	mVersion = 1;
	
}


// 析构函数
CBaseSocket::~CBaseSocket(void)
{
	//关闭socket
	if(mSockHandle)
	{
		closesocket(mSockHandle);
		mSockHandle = 0;
	}


#ifdef WIN32  
	WSACleanup();  
#endif 
}


// 初始会版本
void CBaseSocket::cbs_InitVersion(uint version/* = 1*/)
{
#ifdef WIN32
	WSAData data;

	mVersion = version;
	WSAStartup(MAKEWORD(version,version),&data);
#endif
}


// 获取源地址
char CBaseSocket::cbs_GetSrcIP(SOCKADDR_IN* addr, IP_Addr_t* iat)
{
	if(!addr || !iat) return 0;

	iat->ip1=(addr->sin_addr.S_un.S_addr)&0xff;
	iat->ip2=(addr->sin_addr.S_un.S_addr>>8)&0xff;
	iat->ip3=(addr->sin_addr.S_un.S_addr>>16)&0xff;
	iat->ip4=(addr->sin_addr.S_un.S_addr>>24)&0xff;
	iat->port = ntohs(addr->sin_port);

	return 1;
}
char CBaseSocket::cbs_GetSrcIP(SOCKADDR_IN* addr, CNetAddrEntity* nae)
{
	nae->SetParams(
		(addr->sin_addr.S_un.S_addr)&0xff,
		(addr->sin_addr.S_un.S_addr>>8)&0xff,
		(addr->sin_addr.S_un.S_addr>>16)&0xff,
		(addr->sin_addr.S_un.S_addr>>24)&0xff,
		ntohs(addr->sin_port));
	return 1;
}


// 初始化socket地址信息
char CBaseSocket::cbs_InitSockAddr( IP_Addr_t* nat, SOCKADDR_IN* soca_in)
{
	char reip[20] = {0};
	if(!nat || !soca_in)return 0;

	sprintf_s(reip,sizeof(reip),"%d.%d.%d.%d",nat->ip1,nat->ip2,nat->ip3,nat->ip4);
	memset((void*)soca_in,0,sizeof(SOCKADDR_IN));
	soca_in->sin_addr.S_un.S_addr = inet_addr(reip);
	soca_in->sin_family =AF_INET;
	soca_in->sin_port = htons(nat->port);

	return 1;
}
// 初始化socket地址信息
char CBaseSocket::cbs_InitSockAddr( CNetAddrEntity* nae, SOCKADDR_IN* soca_in)
{
	string ip = "";
	char reip[20] = {0};
	if(!nae || !soca_in)return 0;

	ip = nae->ToString();
	memset((void*)soca_in,0,sizeof(SOCKADDR_IN));
	soca_in->sin_addr.S_un.S_addr = inet_addr(ip.c_str());
	soca_in->sin_family =AF_INET;
	soca_in->sin_port = htons(nae->mPort);

	return 1;
}

/*
// 获取本地IP地址
int CBaseSocket::cbs_GetLocalAddr(IP_Addr_t* nat)
{
	char host[255] = {0}; 
	if(!nat)return 0;

	if(0 != cbs_GetLocalName(host,sizeof(host)))  
		return 0;     

	struct hostent FAR * lpHostEnt=gethostbyname(host);
	if(host==NULL)
	{
		//产生错误
		nat->ip1=nat->ip2=nat->ip3=nat->ip4=0;
		return 0;
	}
	//获取IP
	LPSTR lpAddr=lpHostEnt->h_addr_list[0];
	if(lpAddr)
	{
		struct in_addr inAddr;
		memmove(&inAddr,lpAddr,4);
		nat->ip1=inAddr.S_un.S_un_b.s_b1;
		nat->ip2=inAddr.S_un.S_un_b.s_b2;
		nat->ip3=inAddr.S_un.S_un_b.s_b3;
		nat->ip4=inAddr.S_un.S_un_b.s_b4;

		nat->port = mLocalPort;
	}
	return 1;
}
*/

int CBaseSocket::cbs_GetLocalAddr(CNetAddrEntity* nae)
{
	char host[255] = {0}; 
	if(!nae)return 0;

	if(0 != cbs_GetLocalName(host,sizeof(host)))  
		return 0;     

	struct hostent FAR * lpHostEnt=gethostbyname(host);
	if(host==NULL)
	{
		//产生错
		return 0;
	}
	//获取IP
	LPSTR lpAddr=lpHostEnt->h_addr_list[0];
	if(lpAddr)
	{
		struct in_addr inAddr;
		memmove(&inAddr,lpAddr,4);
		nae->SetParams(
			inAddr.S_un.S_un_b.s_b1,
			inAddr.S_un.S_un_b.s_b2,
			inAddr.S_un.S_un_b.s_b3,
			inAddr.S_un.S_un_b.s_b4,
			mLocalPort);
	}
	return 1;
}


// 获取本地名称
int CBaseSocket::cbs_GetLocalName(char* dst, uint dst_len)
{
	char szHostName[256];
	int nRetCode;

	if(!dst) return 0;

	nRetCode=gethostname(szHostName,sizeof(szHostName));
	if(nRetCode!=0)//产生错误
		return GetLastError();

	memcpy(dst,szHostName,dst_len);
	return 0;
}


// 初始化本地socket信息
char CBaseSocket::cbs_InitLoaclSocket(NetType_e nt, uint localport)
{
	IP_Addr_t iat;

	char tip[20] = {0};

	if(!localport) mLocalPort = localport;

	//先关闭
	if(mSockHandle)
	{
		closesocket(mSockHandle);
		mSockHandle = 0;
	}

	//申请
	if(NT_UDP == nt)
		mSockHandle=socket(AF_INET,SOCK_DGRAM,0);			//基于UDP的socket,用于文本传输
	else
		mSockHandle=socket(AF_INET,SOCK_STREAM,0);		

	if(!mSockHandle){

		return 0;
	}

	//获取本地IP地址
	memset(&iat,0,sizeof(iat));
	//cbs_GetLocalAddr(&iat);

	sprintf(tip,"%d.%d.%d.%d",iat.ip1,iat.ip2,iat.ip3,iat.ip4);
	memset(&mLocalSockAddr,0,sizeof(SOCKADDR_IN));
	mLocalSockAddr.sin_addr.S_un.S_addr = htonl(/*tip*/INADDR_ANY);
	mLocalSockAddr.sin_family =AF_INET;
	mLocalSockAddr.sin_port = htons(iat.port);

	//绑定
	if(SOCKET_ERROR==bind(mSockHandle,(SOCKADDR*)&mLocalSockAddr,sizeof(SOCKADDR))){
		return 0;
	}

	return 1;
}