#ifndef HEADER_SOCKET_BASE_SHI_QINGLIANG
#define  HEADER_SOCKET_BASE_SHI_QINGLIANG


#include <iostream>
#include <CAddressEntity.hpp>


#ifndef WIN32  
#include <sys/types.h>  
#include <sys/wait.h>  
#include <sys/socket.h>  
#include <arpa/inet.h>  
#include <netinet/in.h>  
#include <signal.h>  
#include <netdb.h>  
#include <unistd.h>  
#include <fcntl.h>  
#else  

#include <winsock2.h>  
#pragma comment(lib, "ws2_32.lib")

#endif  
// Linux  
#ifndef WIN32  
#define OP_SOCKET               int  
#define OP_SOCKADDR_IN          struct sockaddr_in  
#define OP_SOCKADDR             struct sockaddr  
#define OP_SOCKLEN_T            socklen_t  
// Windows  
#else  
#define OP_SOCKET               SOCKET  
#define OP_SOCKADDR_IN          SOCKADDR_IN  
#define OP_SOCKADDR             SOCKADDR  
#define OP_SOCKLEN_T            int FAR  
#endif  

using namespace std;

#ifndef uint
#define uint unsigned int
#endif

#ifndef uchar
#define uchar unsigned char
#endif

typedef enum __NetType{
	NT_UDP=0,
	NT_TCP
}NetType_e;

//IP地址
typedef struct __IP_address{

	uchar ip1;
	uchar ip2;
	uchar ip3;
	uchar ip4;
	uint port;

}IP_Addr_t;

#include <Lock.hpp>

class CBaseSocket
{
public:

	/***
	 * @breif: 构造函数、析构函数
	 * @param: null
	 * @return: null
	 */
	CBaseSocket(void);
	~CBaseSocket(void);

protected:

	char m_eixt;		//退出标志
	MyLock m_lock;      //the Lock

public:

	unsigned int	mVersion;		//socket版本
	SOCKET			mSockHandle;	//socket句柄	
	unsigned int	mLocalPort;		//本地端口
	SOCKADDR_IN		mLocalSockAddr; //本地地址

	/***
	 * @breif: 设置socket版本
	 * @param version: 版本
	 * @return: null
	 */
	void cbs_SetVersion(unsigned int version) {
		mVersion = version;
	};


	/***
	 * @brief: 加锁
	 * @param: null
	 * @return: null
	 */
	void cbs_Lock() {m_lock.Lock();};


	/***
	 * @brief: 解锁
	 * @param null
	 * @return null
	 */
	void cbs_UnLock() {m_lock.UnLock();};


	/***
	 * @brief: 初始会版本
	 * @param version: 版本号
	 * @return null
	 */
	void cbs_InitVersion(unsigned int version = 1);


	/***
	 * @brief: 获取源地址
	 * @param addr:
	 * @param iat: 
	 * @return: 0 for failed, 1 for OK.
	 */
	char cbs_GetSrcIP(SOCKADDR_IN* addr, IP_Addr_t* iat);
	char cbs_GetSrcIP(SOCKADDR_IN* addr, CNetAddrEntity* nae);

	/***
	 * @brief: 初始化socket地址信息
	 * @param nat:
	 * @param soca_in
	 */
	char cbs_InitSockAddr( IP_Addr_t* nat, SOCKADDR_IN* soca_in);
	char cbs_InitSockAddr( CNetAddrEntity* nae, SOCKADDR_IN* soca_in);


	/***
	 * 本地端口
	 */
	void cbs_LocalPortSet(uint port) {mLocalPort = port;};

	/***
	 * 获取本地IP地址
	 */
	int cbs_GetLocalAddr(CNetAddrEntity* nae);
	//int cbs_GetLocalAddr(IP_Addr_t* nat);
	
	/***
	 * 获取本地名称
	 */
	int cbs_GetLocalName(char* dst, uint dst_len);


	/***
	 * 初始化本地socket信息
	 * @param nt: socket类型
	 * @param localport:
	 */
	char cbs_InitLoaclSocket(NetType_e nt, uint localport = 0);

};

#endif //HEADER_SOCKET_BASE_SHI_QINGLIANG