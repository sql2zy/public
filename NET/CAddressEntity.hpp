#ifndef HEADER_CNETADDRESSENTITY_SHIQINGLIANG__
#define HEADER_CNETADDRESSENTITY_SHIQINGLIANG__

#include <libs_version.h>
#include <iostream>
#include <string>
#include <sstream>
#include <CUnitCtrlEntity.h>
using namespace std;

#pragma region CSerialPortEntity 
class CSerialPortEntity : public CUnitCtrlEntity
{
public:

	CSerialPortEntity(void){ Zero(); };
	CSerialPortEntity(
		unsigned int comport, 
		unsigned int baudrate, 
		unsigned int check, 
		unsigned int databits,
		unsigned int stopsbits)
	{ 
		Zero();
		SetParams(comport,baudrate,check,databits,stopsbits);
	};


	unsigned int  mComport;		//串口号
	unsigned int  mBaudrate;	//波特率
	char		  mCheck;		//校验
	unsigned int  mDatabits;	//数据位
	unsigned int  mStopsbits;	//停止位

	void Zero()
	{
		mComport = 0;
		mBaudrate = 0;
		mCheck = 'N';
		mDatabits = 0;
		mStopsbits = 0;
	}

	void SetParams(
		unsigned int comport, 
		unsigned int baudrate, 
		unsigned int check, 
		unsigned int databits,
		unsigned int stopsbits){

			mComport	= comport;
			mBaudrate	= baudrate;
			mCheck		= check;
			mDatabits	= databits;
			mStopsbits	= stopsbits;
	};


	void CopyFrom(const CSerialPortEntity& from){

		SetParams(from.mComport,from.mBaudrate,from.mCheck,from.mDatabits,from.mStopsbits);
	};


	CSerialPortEntity& operator = (const CSerialPortEntity& from){

		SetParams(from.mComport,from.mBaudrate,from.mCheck,from.mDatabits,from.mStopsbits);
		return *this;
	};


	const string ToString(bool ex = false){

		stringstream ss;
		ss.str("");
		ss<<"PORT"<<mComport;

		if(ex)
			ss<<":"<<mBaudrate;

		return ss.str();
	};
};
#pragma endregion CSerialPortEntity 

#pragma region CNetAddrEntity 

class CNetAddrEntity : public CUnitCtrlEntity
{
public:

	CNetAddrEntity(void){ Zero(); };
	CNetAddrEntity(unsigned int ip1, unsigned int ip2, unsigned int ip3, unsigned int ip4,unsigned int port)
	{ 
		Zero();
		SetParams(ip1,ip2,ip3,ip4,port);
	};


	unsigned char  mIP1;
	unsigned char  mIP2;
	unsigned char  mIP3;
	unsigned char  mIP4;
	unsigned int mPort;

	void Zero()
	{
		mIP1 = 0;
		mIP2 = 0;
		mIP3 = 0;
		mIP4 = 0;
		mPort = 0;
	}

	void SetParams(unsigned char ip1, unsigned char ip2, unsigned char ip3, unsigned char ip4,unsigned int port){
	
		mIP1 = ip1;
		mIP2 = ip2;
		mIP3 = ip3;
		mIP4 = ip4;
		mPort = port;
	};

	void CopyFrom(const CNetAddrEntity& from){
		SetParams(from.mIP1,from.mIP2,from.mIP3,from.mIP4,from.mPort);
	};

	CNetAddrEntity& operator = (const CNetAddrEntity& from){

		SetParams(from.mIP1,from.mIP2,from.mIP3,from.mIP4,from.mPort);
		return *this;
	};

	const string ToString(bool ex = false){
	
		stringstream ss;
		ss.str("");
		ss<<(int)mIP1<<"."<<(int)mIP2<<"."<<(int)mIP3<<"."<<(int)mIP4;

		if(ex)
			ss<<":"<<(int)mPort;

		return ss.str();
	};
};

#pragma endregion CNetAddrEntity 



#endif