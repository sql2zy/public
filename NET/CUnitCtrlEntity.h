#ifndef HEADER_CUNITCTRLENTITY_SHIQINGLIANG__
#define HEADER_CUNITCTRLENTITY_SHIQINGLIANG__

#include <iostream>
#include <string>

class CUnitCtrlEntity
{
public:

	/***
	 * @brief: structure & destructure
	 */
	CUnitCtrlEntity(void){};
	~CUnitCtrlEntity(void){};

	unsigned int mVersion;	//�汾��

	/***
	 * @brief: �����ַ���
	 */
	virtual const std::string ToString(bool ex = false) = 0;

	virtual void Zero() = 0;
};

#endif