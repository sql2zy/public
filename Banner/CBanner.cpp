#include "libs_version.h"
#include "cwnd/CBanner.h"

#include "cwnd/MemDC.h"

//structure
CBanner::CBanner(void)
{
	mSpeed = 5;
	mTime = 200;
	mDirection = D_HORIZONTAL;
	mLeft = 0;
	mTextIndex = 0;
	mBackgroudColor = RGB(187, 238, 238);
}

//destructrue
CBanner::~CBanner(void)
{}

//
BOOL CBanner::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext /* = NULL */)
{
	if(IsWindow(m_hWnd))
	{
		MoveWindow(&rect);
		return TRUE;
	}
	else
		return CWnd::Create(lpszClassName,lpszWindowName,dwStyle,rect,pParentWnd,nID);
}

//speed
void CBanner::SetSpeed(int s, BOOL refrsh/* = TRUE*/)
{
	mSpeed = s;
	if(TRUE == refrsh)
		ReStart();
}

void CBanner::SetTime(int s, BOOL refrsh /*= TRUE*/)
{
	mTime = s;
	if(TRUE == refrsh)
		ReStart();
}

void CBanner::SetDirection(_BannderDirection_ d, BOOL refrsh/* = TRUE*/)
{
	mDirection = d;
	if(TRUE == refrsh)
		ReStart();
}

// background color
void CBanner::SetBackgroundColor(COLORREF bk, BOOL refrsh/* = TRUE*/)
{
	mBackgroudColor = bk;
	if(TRUE == refrsh)
		ReStart();
}

//自增
void CBanner::TextIndexIncrease(int step/* = 1*/)
{
	mTextIndex += step;
	if(mTextIndex >= mVetor.size())
		mTextIndex = 0;
}

//获取显示文字
string CBanner::GetText()
{
	if(mTextIndex >= mVetor.size())
		mTextIndex = 0;

	if(mVetor.size() > mTextIndex)
		return mVetor.at(mTextIndex);

	return "";
}

//restart
void CBanner::ReStart(int speed/* = 0*/, unsigned int ttime/* = 0*/)
{
	if(0 != speed)
		mSpeed = speed;

	if(ttime > 0)
		mTime = ttime;

	GetClientRect(mClient);
	mLeft = 0;

	KillTimer(1);
	SetTimer(1,mTime,NULL);
}

BEGIN_MESSAGE_MAP(CBanner, CWnd)
ON_WM_TIMER()
ON_WM_PAINT()
END_MESSAGE_MAP()

void CBanner::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(1 == nIDEvent)
		Invalidate();

	CWnd::OnTimer(nIDEvent);
}

//add string
void CBanner::AddString(string message)
{
	mVetor.push_back(message);
}

//delete
void CBanner::ClearString(){mVetor.clear();}

void CBanner::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if(!IsWindow(m_hWnd) || 0 == mVetor.size())
		return ;

	CRect rect(0,0,0,0);
	string text = GetText();
	GetClientRect(mClient);

	CMyMemDC pDC((CDC*)&dc,mClient,mBackgroudColor);
	pDC->SetFont();
	CSize sd = pDC->GetTextExtent(text.c_str());
	GetTargeRect(rect,sd);

	if(D_VERTICAL == mDirection)
		pDC->DrawText(text.c_str(),rect,DT_LEFT);
	else 
		pDC->DrawText(text.c_str(),rect,DT_VCENTER|DT_LEFT|DT_SINGLELINE);
	
	dc.BitBlt(mClient.left,mClient.top,mClient.Width(),mClient.Height(),&pDC,mClient.left,mClient.top,SRCCOPY);
	pDC->DeleteDC();
}

//
void CBanner::GetTargeRect(CRect& rect,const CSize font)
{
	if(D_VERTICAL == mDirection)
	{
		if(mLeft< mClient.Height() + font.cy)
			mLeft += mSpeed;
		else
			mLeft = 0;
		rect.SetRect(mClient.left,mClient.bottom-mLeft,mClient.right,mClient.bottom);
	}
	else if (D_HORIZONTAL == mDirection)
	{
		if(mLeft < mClient.Width() + font.cx)
			mLeft += mSpeed;
		else
			mLeft = 0;

		rect.SetRect(mClient.right - mLeft,mClient.top,mClient.right,mClient.bottom);
	}
	if(0 == mLeft)
		TextIndexIncrease();
}