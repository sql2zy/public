#ifndef HEADER_MEMDC_SHIQINGLIANG__
#define HEADER_MEMDC_SHIQINGLIANG__

#include <afxwin.h>
#include <assert.h>
#pragma warning(once:4244)

class CMyMemDC:public CDC
{
public:
	CMyMemDC(void){Zero();};
	CMyMemDC(CDC* pDC,CRect rect,COLORREF bk = RGB(255,255,255))
	{
		Zero();
		Init(pDC,rect,bk);
	}
	~CMyMemDC(void){
	
		if(mOldBMP && m_hDC){
			SelectObject(mOldBMP);
			mBitmap.DeleteObject();
		}

		if(mOldFont && m_hDC){
			SelectObject(mOldFont);
			mMyFont.DeleteObject();
		}
		DeleteDC();
	};

public:
	
	/***
	 * @brief: 
	 */
	void Zero()
	{
		mOldBMP		= NULL;
		mOldBrush	= NULL;
		mOldFont	= NULL;
	}

	/***
	 * @brief: 
	 */
	void Init(CDC* pDC,CRect rect,COLORREF bk = RGB(255,255,255))
	{
		assert(pDC != NULL);
		CreateCompatibleDC(NULL);
		mBitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());
		mOldBMP = (CBitmap*)SelectObject(&mBitmap);
		FillSolidRect(rect,bk);
	}

	/***
	 *
	 */
	void SetFont(
		int nHeight,		// logical height of font height
		int nWidth,			// logical average character width
		int nEscapement,	// angle of escapement
		int nOrientation,	// base-line orientation angle
		int fnWeight,		// font weight
		DWORD fdwItalic,	// italic attribute flag
		DWORD fdwUnderline, // underline attribute flag
		DWORD fdwStrikeOut, // strikeout attribute flag
		DWORD fdwCharSet,	// character set identifier
		DWORD fdwOutputPrecision,	// output precision
		DWORD fdwClipPrecision,		// clipping precision
		DWORD fdwQuality,			// output quality
		DWORD fdwPitchAndFamily,	// pitch and family
		LPCTSTR lpszFace			// pointer to typeface name string
		)
	{
		if(mOldFont)
		{
			mMyFont.DeleteObject();
		}
		mMyFont.CreateFont(nHeight,nWidth,nEscapement,nOrientation,fnWeight,fdwItalic,fdwUnderline,fdwStrikeOut,fdwCharSet,fdwOutputPrecision,
			fdwClipPrecision,fdwQuality,fdwPitchAndFamily,lpszFace);
		if (mOldFont)
			SelectObject(&mMyFont);
		else
			mOldFont = SelectObject(&mMyFont);
	}

	void SetFont(int nHeight=20,int nWidth=0,int nEscapement=0,int nOrientation=0,int fnWeight=FW_BOLD,LPCTSTR lpszFace=_T("����")){

		SetFont(nHeight,nWidth,nEscapement,nOrientation,fnWeight,FALSE,FALSE,0,ANSI_CHARSET,OUT_DEFAULT_PRECIS,
			CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,	DEFAULT_PITCH | FF_SWISS,lpszFace);
	}

	/***
	 * @brief: draw text
	 */
	void myDrawText(CRect rect,CString text,COLORREF bk,COLORREF font,UINT uFormat)
	{
		FillSolidRect(&rect,bk);
		SetTextColor(font);
		DrawText(text,rect,uFormat);
	}

	// Allow usage as a pointer
	CMyMemDC* operator->() {return this;}

	// Allow usage as a pointer
	operator CMyMemDC*() {return this;}

public:
	CBitmap* mOldBMP;
	CBrush* mOldBrush;

	CBitmap mBitmap;

	CFont mMyFont;
	CFont* mOldFont;
};


#endif	//HEADER_MEMDC_SHIQINGLIANG__