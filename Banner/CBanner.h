#ifndef HEADER_CBANNER_SHIQINGLIANG__
#define HEADER_CBANNER_SHIQINGLIANG__
#include <iostream>
#include <afxwin.h>
#include <vector>
#include <string>
using namespace std;
typedef enum __DIRECTION__{
	D_VERTICAL = 0,
	D_HORIZONTAL
}_BannderDirection_;

class CBanner:public CWnd
{
public:

	/***
	 * @brief: structrue & destructrue
	 */
	CBanner(void);
	~CBanner(void);

protected:

	int					mSpeed;		//移动速度
	unsigned int		mTime;		//刷新时间
	_BannderDirection_	mDirection;	//移动方向
	CRect mClient,mTargeCrect;		//当前区域/文字显示区域
	vector<std::string> mVetor;		//显示信息
	unsigned int		mTextIndex;
	COLORREF	mBackgroudColor;	//背景颜色

	int	mLeft;

	/***
	 * @brief: 获取当前区域
	 */
	void GetTargeRect(CRect& rect,const CSize font);

	/***
	 * @brief: 显示序号
	 */
	void TextIndexIncrease(int step = 1);

	/***
	 * @brief: 获取显示文字
	 */
	string GetText();

public:

	/***
	 * @brief: Create
	 */
	BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext /* = NULL */);

	/***
	 * @brief: 重新开始
	 */
	void ReStart(int speed = 0, unsigned int ttime = 0);

	/***
	 * @brief: 移动速度
	 */
	void SetSpeed(int s, BOOL refrsh = TRUE);
	int GetSpeed(){return mSpeed;};

	/***
	 * @brief: 刷新频率
	 */
	void SetTime(int s, BOOL refrsh = TRUE);
	int GetTime(){return mTime;};

	/***
	 * @brief: set direction
	 */
	void SetDirection(_BannderDirection_ d, BOOL refrsh = TRUE);
	_BannderDirection_ GetDirection(){ return mDirection;};

	/***
	 * @brief: 设置背景颜色
	 */
	void SetBackgroundColor(COLORREF bk, BOOL refrsh = TRUE);

	/***
	 * @brief: 添加数据
	 */
	void AddString(string message);

	/***
	 * @brief: 清楚数据
	 */
	void ClearString();


	DECLARE_MESSAGE_MAP()
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
};

#endif //HEADER_CBANNER_SHIQINGLIANG__