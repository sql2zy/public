#include <libs_version.h>
#include <Combuf.h>
#include <stdarg.h>
#include <iomanip>

//structure
CComBuffer::CComBuffer(void)
{
	Zero();
}

//desturcture
CComBuffer::~CComBuffer(void)
{

}

//zero
void CComBuffer::Zero()
{
	memset(mBody,0,sizeof(mBody));
	mLeng = 0;
}

unsigned int CComBuffer::Append(unsigned char c)
{
	if(mLeng + 1 < BUFFER_MAX)
	{
		mBody[mLeng++] = c;
		return mLeng;
	}

	return 0;
}

unsigned int CComBuffer::Append(unsigned char* str, unsigned int len)
{
	if(str)
	{
		if(mLeng + len < BUFFER_MAX)
		{
			memcpy(mBody+mLeng,str,len);
			mLeng += len;
			return mLeng;
		}
	}

	return 0;
}

unsigned int CComBuffer::Append2(int cut,...)
{
	va_list argp; 
	int argno = 0; 
	int para; 

	va_start(argp, cut); 
	char i=0;
	while (i++<cut) 
	{ 
		para = va_arg( argp,  int);  
		Append((unsigned char)para);
	} 
	va_end( argp ); 
	return 0; 
}


#pragma region PAYLOAD

//structure
CPayload::CPayload(){

}

//destructrue
CPayload::~CPayload(){

}

//
unsigned int CPayload::Payload2String(CComBuffer& cb)
{
	cb.Append2(2,mCommandID & 0xFF00, mCommandID & 0x00FF);//id
	cb.Append(ToString(_COMBUF_(unsigned char*)),com_buf_length());	// command data
	return cb.com_buf_length();
}



#pragma endregion PAYLOAD

//////////////////////////////////////////////////////////////////////////
//
ActionBuffer::ActionBuffer(void)
{
	Clear();
}	

//
ActionBuffer::ActionBuffer(unsigned char srcaddr,unsigned char bsaddr,unsigned short cmdid,unsigned char buaddr,unsigned char bmaddr,unsigned char reservation)
{
	Clear();
	Assgin(srcaddr,bsaddr,cmdid,buaddr,bmaddr,reservation);
}

ActionBuffer::ActionBuffer(unsigned char srcaddr[4],unsigned char bsaddr,unsigned short cmdid,unsigned char buaddr,unsigned char bmaddr,unsigned char reservation)
{
	Clear();
	Assgin(srcaddr,bsaddr,cmdid,buaddr,bmaddr,reservation);
}

//验证
int ActionBuffer::get_crc16 (unsigned char *bufData, unsigned int buflen, unsigned char *pcrc)
{
	int ret = 0;  
	unsigned short CRC = 0xffff;  
	unsigned short mPOLYNOMIAL = 0xa001;  
	unsigned int i, j;  


	if(bufData == NULL || pcrc == NULL)  
	{  
		return -1;  
	}  

	if (buflen == 0)  
	{  
		return ret;  
	}  
	for (i = 0; i < buflen; i++)  
	{  
		CRC ^= bufData[i];  
		for (j = 0; j < 8; j++)  
		{  
			if ((CRC & 0x0001) != 0)  
			{  
				CRC >>= 1;  
				CRC ^= mPOLYNOMIAL;  
			}  
			else  
			{  
				CRC >>= 1;  
			}  
		}  
	}  

	pcrc[0] = (unsigned char)(CRC & 0x00ff);  
	pcrc[1] = (unsigned char)(CRC >> 8);  

	return ret;  
}


void ActionBuffer::Assgin(unsigned char srcaddr,unsigned char bsaddr,unsigned char cmdid,unsigned char buaddr,unsigned char bmaddr,unsigned char reservation)
{
	mSrcAddr[0] = srcaddr;
	mBSAddr = bsaddr;
	mBUAddr = buaddr;
	mBMAddr = bmaddr;
	mRevervation = reservation;
	mCommandID = cmdid;
}

void ActionBuffer::Assgin(unsigned char srcaddr[4],unsigned char bsaddr,unsigned char cmdid,unsigned char buaddr,unsigned char bmaddr,unsigned char reservation)
{
	Assgin(srcaddr[0],bsaddr,buaddr,bmaddr,reservation);

	for(int i=1;i<4;i++)
		mSrcAddr[i] = srcaddr[i];
}

ActionBuffer::~ActionBuffer(void)
{

}

//
void ActionBuffer::Clear()
{
	for(int i=0;i<4;i++)
		mSrcAddr[i] = 0;
	mBSAddr = 0;
	mBUAddr = 0;
	mBMAddr = 0;
	mRevervation = 0;
	__super::Clear();
}

//
unsigned int ActionBuffer::action_length()
{
	aLength = 7 + 2 + com_buf_length() + 2;//目标地址+
	return aLength;
}

bool ActionBuffer::Parse(unsigned char* data, unsigned int len)
{
	bool ret = false;

	if(len <9)return false;

	mSrcAddr[0] = data[0];
	mRevervation = data[1];
	mBSAddr = data[2];
	mBUAddr = data[3];
	mBMAddr = data[4];
	mPayload_len = data[5]*0x100 + data[6];

	if(mPayload_len > 2)
	{
		mCommandID = data[7]*0x100 + data[8];
		Append((unsigned char*)&data[9],mPayload_len - 2);
	}

	mCRC[0] = data[len-2];
	mCRC[1] = data[len-1];

	return (ret = true);
}

//Get DWORD
DWORD ActionBuffer::GetDword(int pos)
{
	DWORD ret = 0;

	if(pos+4<mLeng)
		ret = (DWORD)(mBody[pos]*0x1000000 + mBody[pos+1]*0x10000 + 
		mBody[pos+2]*0x100 + mBody[pos+3]);
	return ret;
}

//Get Word
WORD ActionBuffer::GetWord(int pos)
{
	WORD ret = 0;

	if(pos+1<mLeng)
		ret = (WORD)(mBody[pos]*0x100 + mBody[pos+1]);
	return ret;
}

//Get Byte
int ActionBuffer::GetByte(int pos)
{
	if(pos < mLeng)
		return (int)mBody[pos];
	return 0;
}


//////////////////////////////////////////////////////////////////////////

CStringBuffer::CStringBuffer(void)
{
	Zero();
}

CStringBuffer::CStringBuffer(string str)
{
	Zero();
	convert(str);
}

CStringBuffer::~CStringBuffer(void)
{

}

void CStringBuffer::convert(string str)
{
	for (int i=0; i<str.size(); i++)
	{
		Append((unsigned char)str.substr(i,1)[0]);
	}
}