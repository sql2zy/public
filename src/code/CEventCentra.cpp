#include <libs_version.h>
#include <CEventCentra.h>
#include <iostream>
#include <CSynchronization.hpp>
#include <std/CStd.hpp>
#include <process.h>
using namespace std;
//
CMyMap<string,CEventInterface*> gEventCentraList;
CMyList<CEventNode> gEventNodeList;
CSynchroEvent gEventCentraEvent;

//////////////////////////////////////////////////////////////////////////

CEventNode::CEventNode(){Zero();}
CEventNode::CEventNode(unsigned int EventID, WPARAM wparam, LPARAM lparam, LPVOID data, string key /* = "" */)
{
	Zero();
	Fill(EventID,wparam,lparam,data,key);
}

//
CEventNode::~CEventNode(){};

//
void CEventNode::Zero()
{
	mKey = "";
	mID = 0;
	mWparam = mLparam = 0;
	mLPVoid = NULL;
}

//
void CEventNode::Fill(unsigned int EventID, WPARAM wparam, LPARAM lparam, LPVOID data, string key /* = "" */)
{
	mKey = key;
	mID = EventID;
	mWparam = wparam;
	mLparam = lparam;
	mLPVoid = data;
}

void CEventNode::Call(CEventInterface* inter)
{
	if(inter)inter->do_work(mID,mWparam,mLparam,mLPVoid);
}

//
CEventNode& CEventNode::operator =(const CEventNode& from)
{
	Zero();
	mKey = from.mKey;
	mID = from.mID;
	mWparam = from.mWparam;
	mLparam = from.mLparam;
	mLPVoid = from.mLPVoid;
	return *this;
}

//////////////////////////////////////////////////////////////////////////

//
CEventCentra::CEventCentra(void){
	mHandle = INVALID_HANDLE_VALUE;
	mExit = false;

	unsigned int m_proID = 0;
	if(INVALID_HANDLE_VALUE == mHandle)
	{
		mHandle = (HANDLE)_beginthreadex(NULL,0,CEventCentra::EventRouter,this,0,&m_proID);
	}
};

//
CEventCentra::~CEventCentra(void)
{
	
	if(INVALID_HANDLE_VALUE != mHandle)
	{
		mExit= true;
		gEventCentraEvent.SetSynchroEvent();
		WaitForSingleObject(mHandle,INFINITE);
		CloseHandle(mHandle);
		mHandle = INVALID_HANDLE_VALUE;
	}
}

//add
bool CEventCentra::ec_Add(string key,CEventInterface* ei)
{
	bool ret = false;
	if(!ei)
		return false;

	gEventCentraList.mInsert(key,ei,false);
	ret = true;
	return ret;
}

//delete
bool CEventCentra::ec_Del(string key)
{
	bool ret = false;
	gEventCentraList.mRemove(key);
	gEventCentraEvent.SetSynchroEvent();
	return ret;
}

//
bool CEventCentra::ec_EventEx(string key,unsigned int EventID, WPARAM wparam /* = 0 */, LPARAM lparam /* = 0 */, LPVOID lpVoid /* = NULL */)
{
	CEventNode node(EventID,wparam,lparam,lpVoid,key);
	gEventNodeList.AddHead(node);
	gEventCentraEvent.SetSynchroEvent();
	return true;
}

//
void CEventCentra::ec_Event(unsigned int EventID, WPARAM wparam /* = 0 */, LPARAM lparam /* = 0 */, LPVOID lpVoid /* = NULL */)
{
	CEventNode node(EventID,wparam,lparam,lpVoid);
	gEventNodeList.AddHead(node);
	gEventCentraEvent.SetSynchroEvent();
}

//
unsigned WINAPI CEventCentra::EventRouter(LPVOID lpvoid)
{
	CEventCentra* centra = (CEventCentra*)lpvoid;
	if(centra )
		centra->EventRouter();
	return 1;
}

//
void CEventCentra::EventRouter()
{
	CMyMap<string,CEventInterface*>::iterator iter;
	CEventNode en;
	CEventInterface* inter = NULL;

	while(false == mExit)
	{
		gEventCentraEvent.WaitForEvent();
		gEventCentraEvent.ResetSynchroEvent();
		while(gEventNodeList.GetTail(en))//遍历所有的事件
		{
			inter = NULL;
			if (en.mKey.length() > 0)
			{
				if(gEventCentraList.mFind(en.mKey,inter) && inter)
					en.Call(inter);
			}
			else
			{
				gEventCentraList.Lock();
				for (iter = gEventCentraList.begin();iter != gEventCentraList.end();iter++)
				{
					en.Call(iter->second);
				}
				gEventCentraList.Unlock();
			}
		}
	}
}