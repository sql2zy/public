#include <libs_version.h>
#include <entity/CParamsEntity.h>

//structure
CParamsentity::CParamsentity(void)
{
	Zero();
}

CParamsentity::CParamsentity(unsigned int uid, DWORD wparam, DWORD lparam)
{
	Zero();
	mUID = uid;
	mWparam = wparam;
	mLparam = lparam;
}

//destructure
CParamsentity::~CParamsentity(void)
{

}

//��ʼ��
void CParamsentity::Zero()
{
	mUID = 0;
	mWparam = 0;
	mLparam = 0;
}

//
CParamsentity& CParamsentity::operator =(const CParamsentity &from)
{
	Zero();
	mUID = from.mUID;
	mWparam = from.mWparam;
	mLparam = from.mLparam;
	return *this;
}
