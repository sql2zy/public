#include <libs_version.h>
#include <entity/CDeviceEntity.h>


#pragma region HTTPENTITY

CHttpEntity::CHttpEntity(void)
{
	Zero();
}

CHttpEntity::~CHttpEntity(void)
{

}

void CHttpEntity::Zero()
{
	mUrl = "";
	mInterval = 0;
}

CHttpEntity& CHttpEntity::operator = (const CHttpEntity& from)
{
	Zero();
	mUrl.append(from.mUrl);
	mInterval = from.mInterval;

	return *this;
}

#pragma endregion HTTPENTITY

//structure
CDeviceEntity::CDeviceEntity(void)
{
	Zero();
}

//destructure
CDeviceEntity::~CDeviceEntity(void)
{

}

//初始化数据
void CDeviceEntity::Zero()
{
	mUID			= 0;
	mName			= "";
	mDescription	= "";
	mOutPut			= 0;
	mClassify		= 0;
	mAddr			= "";
	mType		= 0;
	mFunc		= 0;
	mUPSmap		= 0;

	mState		= 0;
	mCurrent	= 0;
	mVoltage	= 0;
	mTemperature = 0;
	mDuration	= 0;
	mWarnings	= "";
	mPvoid      = NULL;
}

//添加
bool CDeviceEntity::AddBrick(CBrickEntity *brick)
{
	if(!brick)return false;

	CBrickEntity* b = GetBick(brick->mUID);
	if(!b)
	{
		mBrickList.push_back(brick);
		return true;
	}
	return false;
}

//获取brick
CBrickEntity* CDeviceEntity::GetBick(unsigned int uid)
{
	CBrickEntity* p = NULL, * tmp = NULL;
	list<CBrickEntity*>::iterator iter;

	for (iter = mBrickList.begin();iter != mBrickList.end();iter++)
	{
		tmp = (*iter);
		if(tmp->mUID == uid)
		{
			p = tmp;
			break;
		}
	}

	return p;
}

//获取brick数量
unsigned int CDeviceEntity::GetBricksCount()
{
	return mBrickList.size();
}