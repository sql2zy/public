#include <libs_version.h>
#include <entity/HttpDataEntity.h>


#pragma region CHttpExceptionEntity

//structure
CHttpExceptionEntity::CHttpExceptionEntity(void)
{
	Zero();
}

CHttpExceptionEntity::CHttpExceptionEntity(string type, int code, string message)
{
	Zero();
	mType = type;
	mCode = code;
	mMessage = message;
}

//destructure
CHttpExceptionEntity::~CHttpExceptionEntity()
{

}

//init
void CHttpExceptionEntity::Zero()
{
	mType = "";
	mCode = 0;
	mMessage = "";
}

CHttpExceptionEntity& CHttpExceptionEntity::operator =(const CHttpExceptionEntity& from)
{
	Zero();
	mType = from.mType;
	mCode = from.mCode;
	mMessage = from.mMessage;

	return *this;
}

#pragma endregion CHttpExceptionEntity

#pragma  region CHttpComDataEntity

//structure
CHttpComDataEntity::CHttpComDataEntity(void)
{
	Zero();
}

//destructure
CHttpComDataEntity::~CHttpComDataEntity(void)
{
	
}

//initilzation
void CHttpComDataEntity::Zero()
{
	mUID = -1;
	mName = "";
	mDescribtion = "";
	mActive = false;
	mVersion = "";
	mVersionUpdatedAt = "";
	mException.Zero();
}

//Copy
CHttpComDataEntity& CHttpComDataEntity::operator =(const CHttpComDataEntity& from)
{
	Zero();

	mUID				= from.mUID;
	mName				= from.mName;
	mDescribtion		= from.mDescribtion;
	mActive				= from.mActive;
	mVersion			= from.mVersion;
	mVersionUpdatedAt	= from.mVersionUpdatedAt;
	mException			= from.mException;

	return *this;
}

#pragma  endregion CHttpComDataEntity


#pragma  region CHttpDeviceEntity
//structure
CHttpDeviceEntity::CHttpDeviceEntity(void)
{
	Zero();
}

//destructure
CHttpDeviceEntity::~CHttpDeviceEntity(void)
{

}

//initilzation
void CHttpDeviceEntity::Zero()
{
	mBelongSiteID = -1;
	mBelongClusterID = -1;
	mUID = -1;
	mNumofBricks = 0;
	mFunc = 0;
	mType = "";
	mName = "";
	mActive = false;
	mVersion = "";
	mVersionUpdatedAt = "";
	mException.Zero();
}

//Copy
CHttpDeviceEntity& CHttpDeviceEntity::operator =(const CHttpDeviceEntity& from)
{
	Zero();
	mBelongSiteID		= from.mBelongSiteID;
	mBelongClusterID	= from.mBelongClusterID;
	mNumofBricks		= from.mNumofBricks;
	mType				= from.mType;
	mName				= from.mName;
	mFunc				= from.mFunc;
	mUID				= from.mUID;
	mName				= from.mName;
	mActive				= from.mActive;
	mVersion			= from.mVersion;
	mVersionUpdatedAt	= from.mVersionUpdatedAt;
	mException			= from.mException;

	return *this;
}

#pragma  endregion CHttpDeviceEntity