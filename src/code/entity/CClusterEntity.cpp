#include <libs_version.h>
#include <entity/CClusterEntity.h>

//structure
CClusterEntity::CClusterEntity(void)
{
	Zero();
}

//destructure
CClusterEntity::~CClusterEntity(void)
{

}

//初始化
void CClusterEntity::Zero()
{
	mUID			= 0;
	mName			= "";
	mDescription	= "";

	mVoltage		= 0;
	mPoewr			= 0;
	mEnergyStorage	= 0;
}


//设置名称
void CClusterEntity::SetName(std::string name)
{
	mName.clear();
	mName.append(name);
}

//设置描述
void CClusterEntity::SetDescription(std::string description)
{
	mDescription.clear();
	mDescription.append(description);
}

//添加device
bool CClusterEntity::AddDevice(CDeviceEntity *device)
{
	CDeviceEntity*p  =  GetDevice(device->mUID);
	if(NULL == p)
	{
		mDeviceList.push_back(*device);
		return true;
	}

	return false;
}

//获取device
CDeviceEntity* CClusterEntity::GetDevice(unsigned int uid)
{
	CDeviceEntity* device = NULL;
	list<CDeviceEntity>::iterator iter;

	for (iter = mDeviceList.begin(); iter != mDeviceList.end(); iter++)
	{
		if ((*iter).mUID == uid)
		{
			device = &(*iter);
			break;
		}
	}

	return device;
}

//获取Device数量
unsigned int CClusterEntity::GetDevicesCount()
{
	return mDeviceList.size();
}

//获取brick数量
unsigned int CClusterEntity::GetBricksCount()
{
	unsigned int ret = 0;
	list<CDeviceEntity>::iterator iter;
	
	for (iter = mDeviceList.begin(); iter != mDeviceList.end(); iter++)
	{
		ret += (*iter).mBrickList.size();
	}

	return ret;
}
