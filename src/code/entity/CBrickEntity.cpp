#include <libs_version.h>
#include <entity/CBrickEntity.h>
#include <sstream>

//structure
CBrickEntity::CBrickEntity(void)
{
	Zero();
}

//destructure
CBrickEntity::~CBrickEntity(void)
{
	mModuleList.clear();
}

//initialize
void CBrickEntity::Zero()
{
	mUID			= 0;
	mName			= "";
	mDescription	= "";
	mSettingVoltage		= mSettingCurrent = 0;
	mRealtimeVoltage	= mRealtimeCurrent = 0;
	mHost			= false;
	mEnableState	= true;
	mBrickState		= 0;
	mErrorCode		= 0;
	mOutPut		= 0;
	mMinUnit	= 0;
	mWorkMode	= -1;
	mSOC		= 0;
	mTemprature	= 0;
	mDuration	= 0;
	memset(mVersion,0,sizeof(mVersion));
	memset(mProductName,0,sizeof(mProductName));
	memset(mSoftVersion,0,sizeof(mSoftVersion));
}

//operator
CBrickEntity& CBrickEntity::operator =(CBrickEntity &b)
{
	Zero();
	mUID = b.mUID;
	mName.append(b.mName);
	mDescription.append(b.mDescription);
	mSettingCurrent		= b.mSettingCurrent;
	mSettingVoltage		= b.mSettingVoltage;
	mRealtimeCurrent	= b.mRealtimeCurrent;
	mRealtimeVoltage	= b.mRealtimeVoltage;
	mHost				= b.mHost;
	mEnableState		= b.mEnableState;
	mBrickState			= b.mBrickState;
	mErrorCode			= b.mErrorCode;
	mWorkMode			= b.mWorkMode;
	mSOC				= b.mSOC;
	mTemprature			= b.mTemprature;
	mDuration			= b.mDuration;
	memcpy(mVersion,b.mVersion,sizeof(mVersion));
	memcpy(mProductName,b.mProductName,sizeof(mProductName));
	memcpy(mSoftVersion,b.mSoftVersion,sizeof(mSoftVersion));
	return *this;
}

//brick状态
bool CBrickEntity::SetState(char state)
{
	mBrickState = state;
	return true;
}

bool CBrickEntity::SetErrorCode(char code)
{
	mErrorCode = code;
	return true;
}

// Get module
CModuleEntity* CBrickEntity::GetModule(unsigned int UID)
{
	CModuleEntity* me = NULL;
	list<CModuleEntity>::iterator iter;

	for (iter = mModuleList.begin(); iter != mModuleList.end(); iter++)
	{
		if(iter->mUID == UID)
		{
			me = &(*iter);
			break;
		}
	}

	return me;
}

//Get Battery
CBatteryEntity* CBrickEntity::GetBattery(unsigned int modID, unsigned int batID)
{
	CModuleEntity* module = NULL;
	CBatteryEntity* battery = NULL;

	try{
		module = GetModule(modID);
		if(module)
			battery = module->GetBattery(batID);
	}
	catch(...)
	{
		cerr<<"An error occured when GetBattery."<<endl;
	}

	return battery;
}

// Add module
bool CBrickEntity::AddModule(CModuleEntity& me, bool replace)
{
	bool ret = false;
	CModuleEntity* p = GetModule(me.mUID);

	if(!p)
	{
		mModuleList.push_back(me);
		ret = true;
	}
	else
	{
		if(true == replace)
		{
			p->CopyFrom(me);
			ret = true;
		}
		else
			ret = false;
	}

	return ret;
}

//Add battery
bool CBrickEntity::AddBattery(unsigned int modID, CBatteryEntity &be, bool replase)
{
	bool ret = false;
	try
	{
		CModuleEntity* p = GetModule(modID);

		if(!p)
		{
			CModuleEntity tmp;
			tmp.AddBattery(be);
			mModuleList.push_back(tmp);
			ret = true;
		}
		else
		{
			p->AddBattery(be);
			ret = true;
		}

	}
	catch(...)
	{
		cerr<<"An error occured when adding battery."<<endl;
	}
	return ret;
}

//////////////////////////////////////////////////////////////////////////
//
//模块使能
bool CBrickEntity::SetModuleEnable(unsigned int modID, bool enable)
{
	bool ret = false;
	try
	{
		CModuleEntity* p = GetModule(modID);
		if(!p)
		{
			p->mPartake = enable;
			ret = true;
		}
		else
			ret = false;
	}
	catch(...)
	{
		cerr<<"An error occured when SetModuleEnable."<<endl;
	}
	return ret;
}

bool CBrickEntity::GetModuleEnable(unsigned int modID)
{
	bool ret = false;
	try
	{
		CModuleEntity* p = GetModule(modID);
		if(!p)
			ret = p->mPartake;
	}
	catch(...)
	{
		cerr<<"An error occured when GetModuleEnable."<<endl;
	}
	return ret;
}

//模块温度
bool CBrickEntity::SetModuleTemprature(unsigned int modID, int temprature)
{
	bool ret = false;
	try
	{
		CModuleEntity* p = GetModule(modID);
		if(!p)
			p->mTemprature = temprature;
	}
	catch(...)
	{
		cerr<<"An error occured when SetModuleTemprature."<<endl;
	}
	return ret;
}

//设置模块的电流电压
bool CBrickEntity::SetModuleCurrent(unsigned int modID, unsigned int current)
{
	bool ret = false;
	try
	{
		CModuleEntity* p = GetModule(modID);
		if(!p)
			p->mCurrent = current;
	}
	catch(...)
	{
		cerr<<"An error occured when SetModuleCurrent."<<endl;
	}
	return ret;
}

//设置模块的电流电压
bool CBrickEntity::SetModuleVoltage(unsigned int modID, unsigned int voltage)
{
	bool ret = false;
	try
	{
		CModuleEntity* p = GetModule(modID);
		if(!p)
			p->mVoltage = voltage;
	}
	catch(...)
	{
		cerr<<"An error occured when SetModuleVoltage."<<endl;
	}
	return ret;
}

//////////////////////////////////////////////////////////////////////////
//设置电池电流
bool CBrickEntity::SetBatteryCurrent(unsigned int modID, unsigned int batID, unsigned int current)
{
	bool ret = false;
	CBatteryEntity* battery = NULL;
	battery = GetBattery(modID,batID);

	if(NULL == battery) return ret;

	battery->mCurrent = current;

	return true;
}

//设置电池电压
bool CBrickEntity::SetBatteryVoltage(unsigned int modID, unsigned int batID, unsigned int voltage)
{
	bool ret = false;
	CBatteryEntity* battery = NULL;
	battery = GetBattery(modID,batID);

	if(NULL == battery) return ret;
	battery->mVoltage = voltage;

	return true;
}

//设置SOC
bool CBrickEntity::SetBatterySOC(unsigned int modID, unsigned int batID, unsigned char soc)
{
	bool ret = false;
	CBatteryEntity* battery = NULL;
	battery = GetBattery(modID,batID);

	if(NULL == battery) return ret;
	battery->mSOC = soc;

	return true;
}

//设置电池状态
bool CBrickEntity::SetBatteryState(unsigned int modID, unsigned int batID, unsigned char state)
{
	bool ret = false;
	CBatteryEntity* battery = NULL;
	battery = GetBattery(modID,batID);

	if(NULL == battery) return ret;
	battery->mState = state;

	return true;
}

//设置电池开关
bool CBrickEntity::SetBatterySwitch(unsigned int modID, unsigned int batID, unsigned char sw)
{
	bool ret = false;
	CBatteryEntity* battery = NULL;
	battery = GetBattery(modID,batID);

	if(NULL == battery) return ret;
	battery->mSwitch = sw;

	return true;
}