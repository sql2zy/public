#include <libs_version.h>
#include <entity/CActionEntity.h>

//structure
CCActionEntity::CCActionEntity(void)
{
	Zero();
}

//destructure
CCActionEntity::~CCActionEntity(void)
{

}

//初始化
void CCActionEntity::Zero()
{
	mLock.Lock();
	mCmd = 0;
	mParameter = 0;
	memset(mBuf,0,sizeof(mBuf));
	mBuf_len = 0;
	mWparam = 0;
	mLparam = 0;
	memset(mData,0,sizeof(mData));
	mState = 0;
	mLock.UnLock();
}

CCActionEntity& CCActionEntity::operator = (const CCActionEntity& from)
{
	Zero();
	mLock.Lock();
	mCmd = from.mCmd;
	mParameter = from.mParameter;
	memcpy(mBuf,from.mBuf,sizeof(mBuf));
	mBuf_len = from.mBuf_len;
	mWparam = from.mWparam;
	mLparam = from.mLparam;
	memcpy(mData,from.mData,sizeof(mData));
	mState = from.mState;
	mLock.UnLock();

	return *this;
}

//锁定
void CCActionEntity::doLock()
{
	mLock.Lock();
}

//解锁
void CCActionEntity::doUnlock()
{
	mLock.UnLock();
}

//设置数值
bool CCActionEntity::SetParams(
							   unsigned int cmd, 
							   unsigned int parameter, 
							   unsigned char *buf, 
							   unsigned int buf_len, 
							   unsigned int wparam,
							   unsigned int lparam, 
							   unsigned char *data, 
							   unsigned int data_len)
{
	bool ret = false;
	if(1 == mState) return ret;

	doLock();
	mCmd = cmd;
	mParameter = parameter;
	
	if(buf && buf_len>0)
	{
		memcpy(mBuf,buf,sizeof(mBuf));
		mBuf_len = buf_len;
	}
	mWparam = wparam;
	mLparam = lparam;
	if (data && data_len > 0)
		memcpy(mData,data,sizeof(mData));
	mState = 1;

	doUnlock();

	return true;
}