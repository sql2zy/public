#include <libs_version.h>
#include <entity/CSiteEntity.h>

//structure
CSiteEntity::CSiteEntity(void)
{
	Zero();
}

//destructure
CSiteEntity::~CSiteEntity(void)
{

}

//initialize
void CSiteEntity::Zero()
{
	mUID			= 0;
	mName			= "";
	mDescription	= "";
	mCommandType	= 0; 

	mState	= 0;
	mPowerCutTimestamp		= 0;
	mPartialOutageTimestamp = 0;
	mFullOutageTimestamp	= 0;
	mDuring			= 0;
	mEnergyStorage	= 0;
	mPower			= 0;
	mWarnings		= "";
}

//设置网址
void CSiteEntity::SetCommandUrl(string url, int interval/* = 2000*/)
{
	mCommandUrl.SetUrl(url);
	mCommandUrl.SetInterval(interval);
}

//添加cluster
bool CSiteEntity::AddCluster(CClusterEntity &cluster)
{
	CClusterEntity* ret = GetCluster(cluster.mUID);

	if(NULL == ret)
	{
		mClusterList.push_back(cluster);
		return true;
	}
	return false;
}

//获取cluster
CClusterEntity* CSiteEntity::GetCluster(unsigned int uid)
{
	CClusterEntity* ret = NULL;
	list<CClusterEntity>::iterator iter;
	for (iter = mClusterList.begin(); iter != mClusterList.end(); iter++)
	{
		if(uid == iter->mUID)
		{
			ret = &(*iter);
			break;
		}
	}

	return ret;
}


// 获取device
CDeviceEntity* CSiteEntity::GetDevice(unsigned int cid, unsigned int did)
{
	CDeviceEntity *d = NULL;
	try
	{
		CClusterEntity*cluster = GetCluster(cid);
		if(cluster)
		{
			d = cluster->GetDevice(did);
		}
	}
	catch(...)
	{

	}

	return d;
}

//获取cluster数量
unsigned int CSiteEntity::GetClustersCount()
{
	return mClusterList.size();
}

//获取device数量
unsigned int CSiteEntity::GetDevicesCount()
{
	list<CClusterEntity>::iterator iter;
	unsigned int ret = 0;

	for (iter = mClusterList.begin(); iter!= mClusterList.end(); iter++)
	{
		ret += iter->GetDevicesCount();
	}

	return ret;
}

unsigned int CSiteEntity::GetBricksCount()
{
	list<CClusterEntity>::iterator iter;
	unsigned int ret = 0;

	for (iter = mClusterList.begin(); iter!= mClusterList.end(); iter++)
	{
		ret += iter->GetBricksCount();
	}

	return ret;
}