
#include <libs_version.h>
#include <entity/CBatteryEntity.h>
#include <sstream>
#include <string>
using namespace std;

//////////////////////////////////////////////////////////////////////////
// things about battery

//structrue
CBatteryEntity::CBatteryEntity(void)
{
	Zero();
}

// destructrue
CBatteryEntity::~CBatteryEntity(void)
{

}

//initialize
void CBatteryEntity::Zero()
{
	mUID = 0;
	mState = 0;
	mPartake = false;
	mBypassSwitch = 0;
	mSwitch = 0;
	mCurrent = 0;
	mVoltage = 0;
	mMask = false;
	mTemprature = 0;

	mSOC = 0;
	mSOH = 0;
	mSize = 0;
	mMaterial = "";
	mPackaging = "";
	mShape = "";
	mName = "";
}

void CBatteryEntity::SetName(string name)
{
	mName.clear();
	mName.append(name);
}

void CBatteryEntity::SetName(int name)
{
	stringstream ss;
	ss.str("");
	ss<<name;
	SetName(ss.str());
}

//����
CBatteryEntity& CBatteryEntity::operator =(CBatteryEntity& u)
{
	Zero();
	mUID = u.mUID;
	mState = u.mState;
	mPartake = u.mPartake;
	mBypassSwitch = u.mBypassSwitch;
	mSwitch = u.mSwitch;
	mCurrent = u.mCurrent;
	mVoltage = u.mVoltage;
	mMask = u.mMask;
	mTemprature = u.mTemprature;

	mSOC = u.mSOC;
	mSOH = u.mSOH;
	mMaterial.append(u.mMaterial);
	mPackaging.append(u.mPackaging);
	mShape.append(u.mShape);

	return *this;
}


//////////////////////////////////////////////////////////////////////////
// things about module
//structure
CModuleEntity::CModuleEntity(void)
{
	Zero();
}
CModuleEntity::CModuleEntity(unsigned int UID)
{
	Zero();
	mUID = UID;
}

//destructure
CModuleEntity::~CModuleEntity(void)
{
	mBatteryList.clear();
}

// initialize
void CModuleEntity::Zero()
{
	mUID = 0;
	mCurrent = 0;
	mVoltage = 0;
	mPartake = false;
	mName = "";
}

//����
CModuleEntity& CModuleEntity::operator =(CModuleEntity &m)
{
	list<CBatteryEntity>::iterator iter;
	mUID = m.mUID;
	mCurrent = m.mCurrent;
	mVoltage = m.mVoltage;
	mTemprature = m.mTemprature;
	mPartake = m.mPartake;

	mLock.Lock();
	try
	{
		for (iter = m.mBatteryList.begin(); iter != m.mBatteryList.end(); iter++)
		{
			CBatteryEntity be;
			be = *(iter);
			mBatteryList.push_back(be);
		}
	}
	catch(...)
	{
		cerr<<"An error occured when copying ModuleEntity, please check it out."<<endl;
	}
	mLock.UnLock();

	return *this;
}

// copy
bool CModuleEntity::CopyFrom(CModuleEntity& m)
{
	bool ret = false;
	list<CBatteryEntity>::iterator iter;
	mUID = m.mUID;
	mCurrent = m.mCurrent;
	mVoltage = m.mVoltage;
	mTemprature = m.mTemprature;
	mPartake = m.mPartake;

	mLock.Lock();
	try
	{
		for (iter = m.mBatteryList.begin(); iter != m.mBatteryList.end(); iter++)
		{
			CBatteryEntity be;
			be = *(iter);
			mBatteryList.push_back(be);
		}
		ret = true;
	}
	catch(...)
	{
		ret = false;
		cerr<<"An error occured when copying ModuleEntity, please check it out."<<endl;
	}
	mLock.UnLock();

	return ret;
}

//Add battery
void CModuleEntity::AddBattery(CBatteryEntity be)
{
	mLock.Lock();
	try{
		mBatteryList.push_back(be);
	}
	catch(...)
	{
		cerr<<"An error occured when Adding battery, please check it out."<<endl;
	}
	mLock.UnLock();
}

//Get battery information
CBatteryEntity* CModuleEntity::GetBattery(unsigned int UID)
{
	list<CBatteryEntity>::iterator iter;
	CBatteryEntity* be = NULL;

	mLock.Lock();

	try
	{
		for (iter = mBatteryList.begin(); iter != mBatteryList.end(); iter++)
		{
			if(iter->mUID == UID)
			{
				be = &(*iter);
				break;
			}
		}
	}
	catch(...)
	{
		cerr<<"An error occured when finding battery from "<<mUID<<", please check it out."<<endl;
	}
	mLock.UnLock();

	return be;
}

void CModuleEntity::SetName(string name)
{
	mName.clear();
	mName.append(name);
}

void CModuleEntity::SetName(int name)
{
	stringstream ss;
	ss.str("");
	ss<<name;
	SetName(ss.str());
}
