#include <libs_version.h>
#include <Cudp.h>

/***
 * @brief: 
 */
static unsigned int WINAPI udp_router(void* lpVoid); 

//
Cudp::Cudp(void){
	mLpvoid = 0;
	mAddition = NULL;
	cbs_InitVersion();
}
Cudp::Cudp(unsigned int port, UDP_CALLBACK recv_cb, UDP_CALLBACK send_cb,unsigned int lpvoid ,void* addtion)
{
	__super::mLocalPort = port;
	mAddition = NULL;
	mLpvoid = 0;
	Assgin(recv_cb,send_cb,lpvoid,addtion);
	cbs_InitVersion();
}

Cudp::~Cudp(void)
{

}

//
bool Cudp::Start(unsigned int port)
{
	bool ret = false;
	if(port) __super::mLocalPort = port;
	if(__super::cbs_InitLoaclSocket(false,__super::mLocalPort))
		return CpStartEx(udp_router,this);

	return false;
}

//
void Cudp::Assgin(UDP_CALLBACK recv_cb, UDP_CALLBACK send_cb,unsigned int lpvoid,void * addtion)
{
	if(recv_cb) mRecvCallback = recv_cb;
	if(send_cb) mSendFaildCallback = send_cb;
	if(addtion) mAddition = addtion;
	mLpvoid = lpvoid;
}

//
unsigned int WINAPI udp_router(void* lpVoid)
{
	Cudp* udp = (Cudp*)lpVoid;
	if(udp)
		udp->run();
	return 1;
}

void Cudp::run()
{
	SOCKADDR_IN tmpAddr;
	char pbuf[2048] = {0} ;
	int ret = 0, len = 0;
	fd_set		gRfd;  //读描述
	timeval		mTo;		//设置超时时间为1秒
	int size = sizeof(tmpAddr);
	string fromip = "";
	unsigned int fromport = 0;

	while(false == m_ProExit)
	{
		mTo.tv_sec = 0;	
		mTo.tv_usec = 1000*20;
		FD_ZERO(&gRfd);

		mLock.do_lock();
		FD_SET(mSockHandle,&gRfd);
		ret = select(0,&gRfd,NULL,NULL,&mTo);
		if(ret> 0)
		{
			memset(pbuf,0,sizeof(pbuf));
			len = recvfrom(mSockHandle,pbuf,sizeof(pbuf),0,(SOCKADDR*)&tmpAddr,&size);
			mLock.do_unlock();
			if(len > 0)
			{
				SOCKADDR_IN* a = (SOCKADDR_IN*)&tmpAddr;
				fromip = ""; fromport = 0;
				cbs_FormatStr(a,fromip,fromport);

				if(mRecvCallback)mRecvCallback(pbuf,len,fromip,fromport,mLpvoid,mAddition);
			}
		}// else if(ret> 0)
		else
		{
			mLock.do_unlock();
			Sleep(100);
		}
	}
}


int Cudp::SendData(string toip, unsigned int toport,char* pdata, unsigned int len)
{
	int ret = 0;
	SOCKADDR_IN tmpAddr;
	int size = sizeof(tmpAddr);
	if (pdata)
	{
		cbs_InitSockAddr(toip,toport,&tmpAddr);
		mLock.do_lock();
		ret = sendto(mSockHandle,pdata,len,0,(SOCKADDR*)&tmpAddr,sizeof(SOCKADDR));
		mLock.do_unlock();
	}
	return ret;
}