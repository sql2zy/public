#include <libs_version.h>
#include <HttpConfig.h>
#include <TimeCom.hpp>


#pragma region CHttpNode

//structrue
CHttpNode::CHttpNode(void)
{
	mHttpCallback = NULL;
}

CHttpNode::CHttpNode(string url, int interval,HTTPCALLBACK hcb)
{
	mUrl.clear();
	mUrl.append(url);
	mInterval = interval;
	if(hcb)	mHttpCallback = hcb;
}

//destructure
CHttpNode::~CHttpNode(void)
{

}

//
unsigned WINAPI CHttpNode::router(void* lpvoid)
{
	CHttpNode* hn = reinterpret_cast<CHttpNode*>(lpvoid);
	if(hn)
		hn->run();

	return 0;
}

//
void CHttpNode::run()
{
	int ret = 0, httpcode = 0;
	string response = "";
	CTimeCom tc;
	tc.cto_reset();
	
	do{
		
		if (tc.cto_check((double)mInterval))
		{
			response.clear();
			ret = Get(mUrl,response,httpcode);
			if(0 == ret && mHttpCallback)
				mHttpCallback(response);
			tc.cto_reset();
		}
		else
			Sleep(20);
	}while (false == m_ProExit);
}

//
bool CHttpNode::Start(string url /* = "" */, int interval /* = 0 */,HTTPCALLBACK hcb)
{
	if(url.size() > 0)
	{
		mUrl.clear();
		mUrl.append(url);
		mInterval = interval;
		if(hcb)
			mHttpCallback = hcb;
	}

	return CpStartEx(router,this);
}

//�˳�
void CHttpNode::Stop()
{
	m_ProExit = true;
}

//operator ==
bool CHttpNode::operator == (const CHttpNode& hn)
{
	return (0 == mUrl.compare(hn.mUrl) ? true : false);
}

//
bool CHttpNode::Check(string str)
{
	return (0 == mUrl.compare(str) ? true : false);
}


#pragma endregion CHttpNode

//////////////////////////////////////////////////////////////////////////

//structrue
CHttpConfig::CHttpConfig(void)
{

}

//destructure
CHttpConfig::~CHttpConfig(void)
{

}

//add client
bool CHttpConfig::AddClient(string url, HTTPCALLBACK cb, int interval /* = 10*100 */)
{
	bool ret = false;
	CHttpNode* node = FindNode(url);

	//update 
	if (node){
		UpdateNode(node,cb,interval);
		return true;
	}

	//add
	node = new CHttpNode(url,interval,cb);
	if(!node)
		node = new CHttpNode(url,interval,cb);

	if(node)
	{
		mLock.do_lock();
		ret = node->Start(url,interval,cb);
		mVector.push_back(node);
		mLock.do_unlock();
	}

	return ret;
}

//delete
bool CHttpConfig::DeleteClient(string url)
{
	vector<CHttpNode*>::iterator iter;
	bool ret = false;
	CHttpNode* node = NULL;
	mLock.do_lock();
	for (int i=0; i< mVector.size(); i++)
	{
		node = mVector.at(i);
		if(node && true == node->Check(url)){
			delete node;
			node = NULL;
			iter = mVector.begin() + i;
			mVector.erase(iter);
			ret = true;
			break;
		}
	}
	mLock.do_unlock();

	return ret;
}

// update
void CHttpConfig::UpdateNode(CHttpNode* node, HTTPCALLBACK cb, int interval)
{
	if(node)
		node->Update(cb,interval);
}

//find http node
CHttpNode* CHttpConfig::FindNode(string url)
{
	CHttpNode* node = NULL;
	mLock.do_lock();
	for (int i=0; i< mVector.size(); i++)
	{
		node = mVector.at(i);
		if(node && true == node->Check(url)){
			break;
		}
		node = NULL;
	}
	mLock.do_unlock();
	return node;
}

//////////////////////////////////////////////////////////////////////////