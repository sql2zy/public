#include <libs_version.h>
#include <struct.h>
#include <entity/CComIDEntity.hpp>
#include <entity/CAddressEntity.hpp>


/***
 * @brief: 获取device
 */
bool GetDevices(CXML* xml, TiXmlElement* pParentElem,CClusterEntity* cluster);

/***
 * @brief: 获取电池信息
 */
bool GetBricks(CXML* xml, TiXmlElement* pParentElem,CDeviceEntity* pDevice);



//构造
CSTRUCT::CSTRUCT(void)
{

}


//析构
CSTRUCT::~CSTRUCT(void)
{

}

//获取配置文件
bool CSTRUCT::ParseConfig(list<CSiteEntity>& filelist)
{
	string key = "";
	bool ret = false;
	CXML xml;
	int var = 0;
	CSiteEntity site;
	TiXmlElement *pParentElem = NULL, *pElement = NULL,*pClildElements = NULL;

	try
	{
		//读取配置文件
		if(!(ret = xml.ParseXmlFile("conf/conf.xml")))
		{
			cerr<<"读取配置文件失败:"<<endl;
			return ret;
		}

		pParentElem = xml.GetRootElement();
		if(!pParentElem) return ret;
		const char* dd = pParentElem->Value();

		pElement = xml.GetFirstChildElement(pParentElem);
		while(pElement)
		{
			site.Zero();
			site.ComID_SetID(xml.GetChildElementValue(pElement,"id",var));			//先获取site信息
			site.ComID_SetName(xml.GetChildElementValue(pElement,"name"));			//名称
			site.ComID_SetDesc(xml.GetChildElementValue(pElement,"description"));	//描述
			site.SetCommandUrl(xml.GetChildElementValue(pElement,"commandurl"));		//网址

			//web地址0.0.0.0:8080
			key = xml.GetChildElementValue(pElement,"webserver");
			sscanf_s(key.c_str(),"%d.%d.%d.%d:%d",&(site.mLocalServer.mIP1),&(site.mLocalServer.mIP2),
				&(site.mLocalServer.mIP3),&(site.mLocalServer.mIP4),&(site.mLocalServer.mPort));

			//web server 运行方式
			site.mCommandType = xml.GetChildElementValue(pElement,"net_command_type",var);	

			//cluster信息
			pClildElements = xml.GetChildElement(pElement,"Clusters");
			dd = pClildElements->Value();
			TiXmlElement *pCluster = xml.GetFirstChildElement(pClildElements);
			
			while(pCluster)
			{
				dd = pCluster->Value();
				CClusterEntity cluster;
				cluster.ComID_SetID(xml.GetChildElementValue(pCluster,"id"));	//ID
				cluster.ComID_SetName(xml.GetChildElementValue(pCluster,"name"));
				cluster.ComID_SetDesc(xml.GetChildElementValue(pCluster,"description"));

				//获取bricks
				TiXmlElement *pDeviceElem = xml.GetChildElement(pCluster,"Devices");
				if(pDeviceElem)
					GetDevices(&xml,pDeviceElem,&cluster);

				pCluster = xml.GetNextChildElement(pCluster);
				site.AddCluster(cluster);
			}//while(pElement)

			pElement = xml.GetNextChildElement(pElement);
			filelist.push_back(site);
		}

	}//try
	catch(exception *e)
	{
		cerr<<"读取配置文件失败:"<<e->what()<<endl;
		ret = false;
	}
	
	return ret;
}

// * @brief: 获取device
bool GetDevices(CXML* xml, TiXmlElement* pParentElem,CClusterEntity* cluster)
{
	int Intversion = 0;
	CDeviceEntity device;

	string key = "", version = "", addr = "";
	TiXmlElement* pElement = NULL, *pAddr = NULL;;
	if(!pParentElem || !xml) return false;

	pElement = xml->GetFirstChildElement(pParentElem);
	while(pElement)
	{
		device.Zero();

		//获取版本
		Intversion = xml->GetElementAttributeValue(pElement,"version",Intversion);
		
		//地址
		addr = xml->GetChildElementValue(pElement,"addr");
		pAddr = xml->GetChildElement(pElement,"addr");
		key = xml->GetElementAttributeValue(pAddr,"type");
		
		if(0 == key.compare("com"))
			device.SetAddress(CAddressEntity::AT_COM,addr);
		else
			device.SetAddress(CAddressEntity::AT_NET,addr);

		device.ComID_SetID(xml->GetChildElementValue(pElement,"id"));				//id	
		device.ComID_SetName(xml->GetChildElementValue(pElement,"name"));			//名称
		device.ComID_SetDesc(xml->GetChildElementValue(pElement,"description"));	//描述
		
		device.mOutPut		= atoi(xml->GetChildElementValue(pElement,"output").c_str());
		device.mClassify	= atoi(xml->GetChildElementValue(pElement,"classify").c_str());
		device.mUPSmap		= atoi(xml->GetChildElementValue(pElement,"upsmap").c_str());

		//获取电池单元
		TiXmlElement* pBricksElem = xml->GetChildElement(pElement,"Bricks");
		GetBricks(xml,pBricksElem,&device);
		//加锁
		pElement = xml->GetNextChildElement(pElement);
		cluster->AddDevice(&device);
	}

	return true;
}

// @brief: 获取电池信息
bool GetBricks(CXML* xml, TiXmlElement* pParentElem,CDeviceEntity* pDevice)
{
	CBrickEntity* obj = NULL;
	string key = "";
	unsigned int minunit = 0;
	bool host = false;
	TiXmlElement* pElement = NULL;
	TiXmlElement* pModuleElement = NULL;;

	pElement = xml->GetFirstChildElement(pParentElem);
	while(pElement)
	{
		obj = NULL;
		obj = new CBrickEntity();
		if(obj)
		{
			obj->ComID_SetID(xml->GetElementAttributeValue(pElement,"id"));	//ID
			key = xml->GetChildElementValue(pElement,"hostclient");			//主机、从机
			obj->mHost = (0 == key.compare("2") ? true : false);
			obj->mMinUnit = (unsigned char)atoi(xml->GetChildElementValue(pElement,"mindischargenumber").c_str());
		
			//
			TiXmlElement* pModulesElem = xml->GetChildElement(pElement,"Modules");
			TiXmlElement* pChiledElement = xml->GetFirstChildElement(pModulesElem);
			while (pChiledElement)
			{
				CModuleEntity module;
				module.mUID = atoi(xml->GetElementAttributeValue(pChiledElement,"id").c_str());
				int batcnt = atoi(xml->GetElementAttributeValue(pChiledElement,"batterys").c_str());
				obj->AddModule(module);

				for (int i=0; i<batcnt; i++)
				{
					CBatteryEntity batter;
					batter.mUID = i;
					obj->AddBattery(module.mUID,batter);
				}
				pChiledElement = xml->GetNextChildElement(pChiledElement);
			}

			pDevice->AddBrick((CBrickEntity*)obj);
		}

		pElement = xml->GetNextChildElement(pElement);
	}
	
	return true;
}


//销毁
void CSTRUCT::Destroy(CSiteEntity *site)
{
	/*	*/
	if(!site) return;
	list<CClusterEntity>::iterator clust_iter;
	list<CDeviceEntity>::iterator devie_iter;
	list<CBrickEntity*>::iterator ups_iter;
	CBrickEntity* brick = NULL;

	for (clust_iter = site->mClusterList.begin(); clust_iter != site->mClusterList.end(); clust_iter++)
	{
		for (devie_iter = clust_iter->mDeviceList.begin(); devie_iter != clust_iter->mDeviceList.end();
			devie_iter++)
		{
			for (ups_iter = devie_iter->mBrickList.begin();ups_iter != devie_iter->mBrickList.end();ups_iter++)
			{
				brick = *ups_iter;
				delete brick;
				brick = NULL;
			}

		}
	}

}


//开始工作
bool CSTRUCT::DoWork(CSiteEntity *site)
{
/*
	list<CClusterEntity>::iterator clust_iter;
	list<CDeviceEntity*>::iterator devie_iter;
	CDeviceEntity* de = NULL;

	CCUnitCtrlInterface* ci = NULL;

	if(!site) return false;

	for (clust_iter = site->mClusterList.begin(); clust_iter != site->mClusterList.end(); clust_iter++)
	{
		for (devie_iter = clust_iter->mDeviceList.begin(); devie_iter != clust_iter->mDeviceList.end();
			devie_iter++)
		{
			ci = (CCUnitCtrlInterface*)((*devie_iter)->GetVoid());
			if(ci)
				ci->Initialize();
		}//devie_iter
	}//clust_iter
	*/
	return true;
}
