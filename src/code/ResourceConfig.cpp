#include <libs_version.h>
#include <ResourceConfig.h>
#include <struct.h>
#include <CMessageCenter.h>

//structrue
CResourceConfig::CResourceConfig(void){

}

//destructre
CResourceConfig::~CResourceConfig(void){

	CSiteEntity* site = NULL;
	list<CSiteEntity>::iterator iter;
	for (iter = mConfigList.begin(); iter!=mConfigList.end();iter++)
	{
		site = &(*iter);
		CSTRUCT::Destroy(site);
	}

}

//parse config file
bool CResourceConfig::MapConfig()
{
	return CSTRUCT::ParseConfig(mConfigList);
}

//获取site对象
CSiteEntity* CResourceConfig::rc_GetSite(unsigned int index)
{
	CSiteEntity* ret = NULL;
	list<CSiteEntity>::iterator iter;
	if (index < mConfigList.size())
	{
		int i= 0;
		for (iter = mConfigList.begin(); iter != mConfigList.end();iter++)
		{
			if (i == index)
			{
				ret = &(*iter);
				break;
			}
			i++;
		}
	}
	return ret;
}

//start
bool CResourceConfig::rc_Start()
{
	list<CClusterEntity>::iterator cluster_iter;
	list<CDeviceEntity>::iterator device_iter;

	CSiteEntity* site = NULL;
	for (int i=0; i< mConfigList.size(); i++)
	{
		site = rc_GetSite(i);
		if (site)
			rc_StartClusters(site);
	}
	return true;
}

//start cluster
bool CResourceConfig::rc_StartClusters(CSiteEntity* site)
{
	list<CClusterEntity>::iterator iter;
	CClusterEntity* cluster = NULL;
	if (site)
	{
		for (iter = site->mClusterList.begin();iter != site->mClusterList.end();iter++)
		{
			cluster = (CClusterEntity*)&(*iter);
			rc_StartDevices(cluster);
		}
	}
	return true;
}

//
bool CResourceConfig::rc_Start(CDeviceEntity* device)
{
	unsigned int handle = 0;
	bool ret = false;
	if(device)
	{
		if (device->mAT == CAddressEntity::AT_COM)
		{
			int com = 0,baud = 0,databits = 0, stopbits = 0;
			char check = 0;
			sscanf(device->mAddress.c_str(),"%d,%d,%c,%d,%d",&com,&baud,&check,&databits,&stopbits);
			handle = CMessageCenter::GetInstance()->mc_Addcom(com,baud,check,databits,stopbits,device);
			if(0 == handle)
				_trace("add %d,%d,%c,%d,%d failed\n",com,baud,check,databits,stopbits);
			else
				device->SetHandle(handle);
		}
		else if (device->mAT == CAddressEntity::AT_NET)
		{
			string ip = "", temp = "";
			int port = 0;

			ip = device->mAddress.substr(0,device->mAddress.find(':'));
			temp = device->mAddress.substr(device->mAddress.find(':')+1,device->mAddress.size());
			port = atoi(temp.c_str());
			handle = CMessageCenter::GetInstance()->mc_Addclient(ip,port,device);

			if(0 == handle)
				_trace("add %s:%d failed\n",ip.c_str(),port);
			else
				device->SetHandle(handle);
		}
	}

	return ret;
}

//start device
bool CResourceConfig::rc_StartDevices(CClusterEntity* cluster)
{
	list<CDeviceEntity>::iterator iter;
	CDeviceEntity* device = NULL;
	if (cluster)
	{
		for (iter = cluster->mDeviceList.begin();iter != cluster->mDeviceList.end();iter++)
		{
			device = (CDeviceEntity*)&(*iter);
			rc_Start(device);
		}
	}
	return true;
}

// 获取device对象
CDeviceEntity* CResourceConfig::rc_GetDevice(unsigned int sid, unsigned int cid, unsigned int did)
{
	CSiteEntity* site = NULL;
	CDeviceEntity* device = NULL;
	//list<CSiteEntity> mConfigList;	//列表
	list<CSiteEntity>::iterator iter;
	for(iter = mConfigList.begin();iter!= mConfigList.end();iter++)
	{
		site = (CSiteEntity*)&(*iter);
		if (sid == site->mUID)
			return site->GetDevice(cid,did);
	}
	return NULL;
}
CDeviceEntity* CResourceConfig::rc_GetDevice(CIdenEntity* ie)
{
	return NULL == ie ? NULL : rc_GetDevice(
		(unsigned int)ie->GetID(IE_SITE_ID),
		(unsigned int)ie->GetID(IE_CLUSTER_ID),
		(unsigned int)ie->GetID(IE_DEVICE_ID));
}