#include <libs_version.h>

#include <BaseNet.h>
#include <sstream>
#include <_trace.h>


// 构造函数
CBaseSocket::CBaseSocket(void)
{
	mVersion = 1;
}


// 析构函数
CBaseSocket::~CBaseSocket(void)
{
	//关闭socket
	if(mSockHandle)
	{
		closesocket(mSockHandle);
		mSockHandle = 0;
	}

#ifdef WIN32  
	WSACleanup();  
#endif 
}


// 初始会版本
void CBaseSocket::cbs_InitVersion(unsigned int version/* = 1*/)
{
#ifdef WIN32
	WSAData data;
	mVersion = version;
	WSAStartup(MAKEWORD(version,version),&data);
#endif
}

bool CBaseSocket::cbs_FormatStr(SOCKADDR_IN* addr,string& ip, unsigned int& port)
{
	stringstream ss;
	bool ret = false;
	if(addr)
	{
		ss.str("");
		ss<<((addr->sin_addr.S_un.S_addr)&0xff)<<"."<<((addr->sin_addr.S_un.S_addr>>8)&0xff)<<"."<<
			((addr->sin_addr.S_un.S_addr>>16)&0xff)<<"."<<((addr->sin_addr.S_un.S_addr>>24)&0xff);
		port = ntohs(addr->sin_port);
		ip = ss.str();
		ret = true;
	}
	return ret;
}

 bool CBaseSocket::cbs_InitSockAddr(const string ip, const unsigned int port, SOCKADDR_IN* soca_in)
 {
	 bool ret = false;
	 if (soca_in)
	 {
		memset((void*)soca_in,0,sizeof(SOCKADDR_IN));
		soca_in->sin_addr.S_un.S_addr = inet_addr(ip.c_str());
		soca_in->sin_family =AF_INET;
		soca_in->sin_port = htons(port);
		ret = true;
	 }

	 return ret;
 }

string CBaseSocket::cbs_GetLocalAddr(int index )
{
	stringstream ss;
	string ret = "", ip = "";
	char host[255] = {0}; 
	ip = cbs_GetLocalName();
	
	if(0 == ip.size()) return ip;     
	struct hostent FAR * lpHostEnt=gethostbyname(host);
	if(host==NULL)	return ret;

	//获取IP
	if(index < lpHostEnt->h_length)
	{
		LPSTR lpAddr=lpHostEnt->h_addr_list[index];
		if(lpAddr)
		{
			struct in_addr inAddr;
			memmove(&inAddr,lpAddr,4);
			ss.str("");
			ss<<inAddr.S_un.S_un_b.s_b1<<"."<<inAddr.S_un.S_un_b.s_b2<<"."<<
				inAddr.S_un.S_un_b.s_b3<<"."<<inAddr.S_un.S_un_b.s_b4;
			
			ret = ss.str();
		}
	}
	return ret;
}


// 获取本地名称
string CBaseSocket::cbs_GetLocalName()
{
	char szHostName[256] = {0};
	string ret = "";
	int nRetCode = gethostname(szHostName,sizeof(szHostName));
	if(nRetCode!=0)//产生错误
		return ret;

	ret.append(szHostName);
	return ret;
}


// 初始化本地socket信息
char CBaseSocket::cbs_InitLoaclSocket(bool tcp, unsigned int localport)
{
	char tip[20] = {0};
	if(!localport) mLocalPort = localport;

	//先关闭
	if(mSockHandle)
	{
		closesocket(mSockHandle);
		mSockHandle = 0;
	}

	//申请
	if(false == tcp)
		mSockHandle=socket(AF_INET,SOCK_DGRAM,0);			//基于UDP的socket,用于文本传输
	else
		mSockHandle=socket(AF_INET,SOCK_STREAM,0);		

	if(!mSockHandle){
		return 0;
	}

	//获取本地IP地址

	string localip = cbs_GetLocalAddr();

	memset(&mLocalSockAddr,0,sizeof(SOCKADDR_IN));
	mLocalSockAddr.sin_addr.S_un.S_addr = htonl(/*tip*/INADDR_ANY);
	mLocalSockAddr.sin_family =AF_INET;
	mLocalSockAddr.sin_port = htons(mLocalPort);

	//绑定
	if(SOCKET_ERROR==bind(mSockHandle,(SOCKADDR*)&mLocalSockAddr,sizeof(SOCKADDR))){
		_printf_err();
		return 0;
	}

	return 1;
}