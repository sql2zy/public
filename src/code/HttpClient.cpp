#include <libs_version.h>
#include <HttpClient.h>

extern "C"
{
	#include "../include/curl/curl.h"
};

typedef struct __put_data
{
	char *data;
	size_t len;
}put_data_t;


#pragma comment(lib,"libcurld_imp.lib")

//构造函数
CHttpClient::CHttpClient(void)
{
	int res = 0;
	res = curl_global_init(CURL_GLOBAL_ALL);
	m_bDebug = false;
}


//析构函数
CHttpClient::~CHttpClient(void)
{

}


//发送数据
bool CHttpClient::http_SendData(char* data, unsigned len)
{
	bool ret = false;


	return ret;
}


//发送数据
bool CHttpClient::http_SendData(string message)
{
	string res;
	bool ret = false;

	if(Put("http://123.57.251.129:8888/nova/rest/protobuf/123",message,res))
	{
		fprintf(stdout,res.c_str());
		return true;
	}
	return ret;
}

static int OnDebug(CURL *, curl_infotype itype, char * pData, size_t size, void *)
{
	if(itype == CURLINFO_TEXT)
	{
		//printf("[TEXT]%s\n", pData);
	}
	else if(itype == CURLINFO_HEADER_IN)
	{
		printf("[HEADER_IN]%s\n", pData);
	}
	else if(itype == CURLINFO_HEADER_OUT)
	{
		printf("[HEADER_OUT]%s\n", pData);
	}
	else if(itype == CURLINFO_DATA_IN)
	{
		printf("[DATA_IN]%s\n", pData);
	}
	else if(itype == CURLINFO_DATA_OUT)
	{
		printf("[DATA_OUT]%s\n", pData);
	}
	return 0;
}

static size_t OnWriteData(void* buffer, size_t size, size_t nmemb, void* lpVoid)
{
	std::string* str = dynamic_cast<std::string*>((std::string *)lpVoid);
	if( NULL == str || NULL == buffer )
	{
		return -1;
	}

    char* pData = (char*)buffer;
    str->append(pData, size * nmemb);
	return nmemb;
}

int CHttpClient::Post(const std::string & strUrl, const std::string & strPost, std::string & strResponse)
{
	CURLcode res;
	CURL* curl = curl_easy_init();
	if(NULL == curl)
	{
		return CURLE_FAILED_INIT;
	}
	if(m_bDebug)
	{
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
		curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, OnDebug);
	}
	curl_easy_setopt(curl, CURLOPT_URL, strUrl.c_str());
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strPost.c_str());
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, OnWriteData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&strResponse);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 3);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
	res = curl_easy_perform(curl);
	curl_easy_cleanup(curl);
	return res;
}

#define TMPFILE 0

static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *userdata)
{
#if TMPFILE
	 size_t retcode;
  curl_off_t nread;
 
  /* in real-world cases, this would probably get this data differently
     as this fread() stuff is exactly what the library already would do
     by default internally */ 
  retcode = fread(ptr, size, nmemb, (FILE*)userdata);
 
  nread = (curl_off_t)retcode;
 
  fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T
          " bytes from file\n", nread);
 
  return retcode;
#else
	put_data_t* pdt = (put_data_t*)userdata;
	size_t curl_size = nmemb * size;
	size_t to_copy = (pdt->len < curl_size) ? pdt->len : curl_size;
	memcpy(ptr, pdt->data, to_copy);
	pdt->len -= to_copy;
	pdt->data += to_copy;
	return to_copy;
#endif
}


// PUT
int CHttpClient::Put(const std::string & strUrl, const std::string & strPut, std::string & strResponse)
{
	char* data = NULL;
	put_data_t m_pdt;
	char tmp[31] = {0};

	CURLcode res;
	CURL* curl = curl_easy_init();

	struct curl_slist *headers=NULL; // init to NULL is important 
	sprintf(tmp,"Content-Length: %d",strPut.size());
	headers = curl_slist_append(headers, "Accept: */*");
	headers = curl_slist_append(headers, "Content-Type: application/x-protobuf; charset=UTF-8"); 
	headers = curl_slist_append(headers, "charset: utf-8");
	//headers = curl_slist_append(headers, tmp);

#if TMPFILE
	FILE* fd = tmpfile();
	fwrite(strPut.c_str(),sizeof(char),strPut.size(),fd);
	fseek(fd,0L,SEEK_SET);
#else
	
	int len = strPut.size();

	data = new char[strPut.size()+1];
	memset(data,0,strPut.size());
	memcpy(data,strPut.c_str(),strPut.size());
	len = strlen(data);
	m_pdt.data = data;
	m_pdt.len = strPut.size();
	
#endif
	
	if(NULL == curl)
	{
		return CURLE_FAILED_INIT;
	}
	if(m_bDebug)
	{
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
		curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, OnDebug);
	}
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers); 
	curl_easy_setopt(curl, CURLOPT_URL, strUrl.c_str());
	//curl_easy_setopt(curl, CURLOPT_PUT, 1L);
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
	 curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strPut);
	 curl_easy_setopt(curl,CURLOPT_POSTFIELDSIZE, strPut.size());
	//curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L); 
	


	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, OnWriteData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&strResponse);
	curl_easy_setopt(curl, CURLOPT_HTTP_TRANSFER_DECODING, 0);

	curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
	
#if TMPFILE
	curl_easy_setopt(curl, CURLOPT_READDATA, fd);
	curl_easy_setopt(curl, CURLOPT_INFILE, fd);
	curl_easy_setopt(curl, CURLOPT_INFILESIZE, strPut.size()); 
#else

	curl_easy_setopt(curl, CURLOPT_READDATA, (void*)&m_pdt);

#endif
/**/

	//curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	//curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 3);
	//curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);

	res = curl_easy_perform(curl);
	curl_slist_free_all(headers);

	int HTTP_flag = 0;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE , &HTTP_flag);

	curl_easy_cleanup(curl);

	if(CURLE_OK == res)
	{
		fprintf(stderr,"发送成功 flag:%d\n",HTTP_flag);
	}
	else
		fprintf(stderr,"发送失败 flag:%d\n",HTTP_flag);

#if TMPFILE
	fclose(fd);
#else
	delete data;
#endif
	return res;
}


//
int CHttpClient::Get(const std::string & strUrl, std::string & strResponse,int& httpflag)
{
	CURLcode res;
	CURL* curl = curl_easy_init();
	if(NULL == curl)
	{
		return CURLE_FAILED_INIT;
	}
	if(m_bDebug)
	{
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
		curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, OnDebug);
	}
	curl_easy_setopt(curl, CURLOPT_URL, strUrl.c_str());
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, OnWriteData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&strResponse);
	/**
	* 当多个线程都使用超时处理的时候，同时主线程中有sleep或是wait等操作。
	* 如果不设置这个选项，libcurl将会发信号打断这个wait从而导致程序退出。
	*/
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 3);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
	res = curl_easy_perform(curl);

	
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE , &httpflag);

	curl_easy_cleanup(curl);
	return res;
}

int CHttpClient::Posts(const std::string & strUrl, const std::string & strPost, std::string & strResponse, const char * pCaPath)
{
	CURLcode res;
	CURL* curl = curl_easy_init();
	if(NULL == curl)
	{
		return CURLE_FAILED_INIT;
	}
	if(m_bDebug)
	{
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
		curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, OnDebug);
	}
	curl_easy_setopt(curl, CURLOPT_URL, strUrl.c_str());
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strPost.c_str());
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, OnWriteData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&strResponse);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	if(NULL == pCaPath)
	{
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);
	}
	else
	{
		//缺省情况就是PEM，所以无需设置，另外支持DER
		//curl_easy_setopt(curl,CURLOPT_SSLCERTTYPE,"PEM");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_easy_setopt(curl, CURLOPT_CAINFO, pCaPath);
	}
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 3);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
	res = curl_easy_perform(curl);
	curl_easy_cleanup(curl);
	return res;
}

int CHttpClient::Gets(const std::string & strUrl, std::string & strResponse, const char * pCaPath)
{
	CURLcode res;
	CURL* curl = curl_easy_init();
	if(NULL == curl)
	{
		return CURLE_FAILED_INIT;
	}
	if(m_bDebug)
	{
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
		curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, OnDebug);
	}
	curl_easy_setopt(curl, CURLOPT_URL, strUrl.c_str());
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, OnWriteData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&strResponse);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	if(NULL == pCaPath)
	{
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);
	}
	else
	{
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_easy_setopt(curl, CURLOPT_CAINFO, pCaPath);
	}
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 3);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
	res = curl_easy_perform(curl);
	curl_easy_cleanup(curl);
	return res;
}