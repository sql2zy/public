#ifdef _WIN32
#define _BIND_TO_CURRENT_VCLIBS_VERSION 1
#endif
#include "CTimer.h"
#include <process.h>
#include <TimeCom.hpp>

//////////////////////////////////////////////////////////////////////////
//
//structrue
CTimerNode::CTimerNode(){Zero();}
CTimerNode::CTimerNode(CTimerInterface* cti,unsigned int id,DWORD dwMilliSeconds, DWORD wparam, DWORD lparam, LPVOID lpvoid)
{
	Zero();
	Fill(cti,id,dwMilliSeconds,wparam,lparam,lpvoid);
}

CTimerNode::CTimerNode(TIMER_CALLBACK cb,unsigned int id,DWORD dwMilliSeconds, DWORD wparam, DWORD lparam, LPVOID lpvoid)
{
	Zero();
	Fill(cb,id,dwMilliSeconds,wparam,lparam,lpvoid);
}
//destructrue
CTimerNode::~CTimerNode(void){}

//clear
void CTimerNode::Zero()
{
	mID = 0;
	wParam = lParam = 0;
	mInterface = NULL;
	lpVoid = NULL;
	mCallback = NULL;
	mType = 0;
}

//fill
void CTimerNode::Fill(CTimerInterface* cti,unsigned int id,DWORD dwMilliSeconds, DWORD wparam, DWORD lparam, LPVOID lpvoid)
{
	mInterface = cti;
	mCallback = NULL;
	mID = id;
	wParam = wparam;
	lParam = lparam;
	lpVoid = lpvoid;
	mdwMilliSeconds = dwMilliSeconds;
	GetLocalTime(&mStart);
	mType = 0;
}

void CTimerNode::Fill(TIMER_CALLBACK cb,unsigned int id,DWORD dwMilliSeconds, DWORD wparam, DWORD lparam, LPVOID lpvoid)
{
	mInterface = NULL;
	mCallback = cb;
	mID = id;
	wParam = wparam;
	lParam = lparam;
	lpVoid = lpvoid;
	mdwMilliSeconds = dwMilliSeconds;
	GetLocalTime(&mStart);
	mType = 1;
}

CTimerNode& CTimerNode::operator = (const CTimerNode& from)
{
	Zero();
	mInterface = from.mInterface;
	mID = from.mID;
	wParam = from.wParam;
	lParam = from.lParam;
	lpVoid = from.lpVoid;
	mCallback = from.mCallback;
	mdwMilliSeconds = from.mdwMilliSeconds;
	mStart = from.mStart;
	return *this;
}

//
bool CTimerNode::operator == (const CTimerNode& node) const
{
	bool ret = false;
	if(mType == node.mType)
	{
		if(0 == mType)
			ret = (mInterface == node.mInterface) ? true : false;
		else
			ret = (mCallback == node.mCallback) ? true : false;
	}
	if(true == ret)
	{
		ret = (mID == node.mID) ? true : false;
	}
	return ret;
}

bool CTimerNode::Call()
{
	bool ret = false;
	CTimeCom tc;
	ret = tc.cto_check(&mStart,mdwMilliSeconds);
	if (ret)
	{
		if(mInterface)
			mInterface->Timer(mID,wParam,lParam,lpVoid);
		else if(mCallback)
			mCallback(mID,wParam,lParam,lpVoid);

		GetLocalTime(&mStart);
	}
	return ret;
}


//////////////////////////////////////////////////////////////////////////
//
//structrue
CTimer::CTimer(void)
{
	mTimerHandle = INVALID_HANDLE_VALUE;
	mExit = false;
}

//destructrue
CTimer::~CTimer(void)
{
	mExit = true;
	if(INVALID_HANDLE_VALUE != mTimerHandle)
	{
		WaitForSingleObject(mTimerHandle,INFINITE);
		CloseHandle(mTimerHandle);
		mTimerHandle = INVALID_HANDLE_VALUE;
	}

}

//
unsigned WINAPI CTimer::CTimer_Router(void* lpvoid)
{
	if(lpvoid)
	{
		CTimer* timer = static_cast<CTimer*>(lpvoid);
		timer->router();
	}
	return 1;
}

//
void CTimer::router()
{
	CTimerNode node;
	std::vector<CTimerNode>::iterator iter;
	while(false == mExit)
	{
		mTimerLock.do_lock();
		for (iter = mVect.begin(); iter != mVect.end(); iter++)
		{
			iter->Call();
		}
		mTimerLock.do_unlock();
		Sleep(10);
	}
}

//
void CTimer::CheckRouter()
{
	unsigned int m_proID = 0;
	if(INVALID_HANDLE_VALUE == mTimerHandle)
	{
		mTimerHandle = (HANDLE)_beginthreadex(NULL,0,CTimer::CTimer_Router,this,0,&m_proID);
	}
}

//
bool CTimer::SetTimer(CTimerInterface* cti,DWORD dwMilliSeconds, unsigned int TimerID /* = 0 */, DWORD wparam /* = 0 */, DWORD lparam /* = 0 */, LPVOID lpVoid /* = 0 */)
{
	CTimerNode node(cti,TimerID,dwMilliSeconds,wparam,lparam,lpVoid);
	return SetTimer(&node);
}

//
bool CTimer::SetTimer(TIMER_CALLBACK tb,DWORD dwMilliSeconds, unsigned int TimerID /* = 0 */, DWORD wparam /* = 0 */, DWORD lparam /* = 0 */, LPVOID lpVoid /* = 0 */)
{
	CTimerNode node(tb,TimerID,dwMilliSeconds,wparam,lparam,lpVoid);
	return SetTimer(&node);
}

//add timer
bool CTimer::SetTimer(CTimerNode* tn)
{
	bool ret = false;
	std::vector<CTimerNode>::iterator iter;
	CheckRouter();
	if(INVALID_HANDLE_VALUE != mTimerHandle && NULL != tn)
	{
		mTimerLock.do_lock();
		for (iter = mVect.begin(); iter != mVect.end(); iter++)
		{
			if(*iter == *tn)
			{
				iter->mdwMilliSeconds = tn->mdwMilliSeconds;
				ret = true;
				break;
			}
		}
		if(false == ret)
			mVect.push_back(*tn);
		mTimerLock.do_unlock();
		return true;
	}
	return false;
}

// remove timer
bool CTimer::KillTimer(CTimerInterface* cti, unsigned int TimerID)
{
	CTimerNode node(cti,TimerID,0);
	return KillTimer(&node);
}

//remove timer
bool CTimer::KillTimer(TIMER_CALLBACK tb, unsigned int TimerID)
{
	CTimerNode node(tb,TimerID,0);
	return KillTimer(&node);
}

// remove timer
bool CTimer::KillTimer(CTimerNode* tn)
{
	bool ret = false;
	CTimerNode node;
	std::vector<CTimerNode>::iterator iter;
	if(false == mExit)
	{
		mTimerLock.do_lock();
		for (iter = mVect.begin(); iter != mVect.end();)
		{
			if (*tn == *iter){
				ret = true;			
				iter = mVect.erase(iter);
				break;
			}
			else
				++iter;
		}
		mTimerLock.do_unlock();
	}

	return ret;
}