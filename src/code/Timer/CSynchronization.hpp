#ifndef HEADER_CSYNCHRONIZATION_SHIQINGLIANG__
#define HEADER_CSYNCHRONIZATION_SHIQINGLIANG__
#include <libs_version.h>
#include <Windows.h>

//////////////////////////////////////////////////////////////////////////
// Clock

class CSynchroLock
{
public:
	CSynchroLock(void){InitializeCriticalSection(&mCS);};
	~CSynchroLock(void){DeleteCriticalSection(&mCS);};

private:
	CRITICAL_SECTION mCS;

public:

	void do_lock(){EnterCriticalSection(&mCS);};
	void do_unlock(){LeaveCriticalSection(&mCS);};

	void work(){do_lock();};
	void unwork(){do_unlock();};
};

//////////////////////////////////////////////////////////////////////////
//
class CSynchroEvent
{
public: 
	CSynchroEvent(void){
		closeEvent();
		mEvnet = CreateEvent(NULL,TRUE,FALSE,NULL);
	};

	~CSynchroEvent(void){closeEvent();};

private:
	HANDLE mEvnet;

	void closeEvent(){
		if(INVALID_HANDLE_VALUE != mEvnet){
			CloseHandle(mEvnet);
			mEvnet = INVALID_HANDLE_VALUE;
		}
	}

public:

	BOOL SetSynchroEvent(){
		
		return INVALID_HANDLE_VALUE == mEvnet ? FALSE : SetEvent(mEvnet);
	}

	BOOL ResetSynchroEvent(){

		return INVALID_HANDLE_VALUE == mEvnet ? FALSE : ResetEvent(mEvnet);
	}

	DWORD WaitForEvent(DWORD dwMilliseconds = INFINITE){

		return INVALID_HANDLE_VALUE == mEvnet ? WAIT_FAILED : WaitForSingleObject(mEvnet,dwMilliseconds);
	}

};

#endif //HEADER_CSYNCHRONIZATION_SHIQINGLIANG__