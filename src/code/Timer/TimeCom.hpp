#ifndef HEADER_TIMEOUT_SHIQINGLIANG__
#define HEADER_TIMEOUT_SHIQINGLIANG__
#include <libs_version.h>
#include <iostream>
#include <time.h>
using namespace std;


class CTimeCom
{
public:

	/***
	 * @brief:
	 */
	CTimeCom(void)
	{
		m_diff = 0;
	}

	~CTimeCom(void)
	{
	}

protected:
	__int64 m_diff;

	SYSTEMTIME m_StBegin;
	SYSTEMTIME m_StEnd;


public:

	/***
	 * @brief reset
	 */
	void cto_reset(){
		GetLocalTime(&m_StBegin);
	}

	/***
	 * @brief: zero
	 */
	void cto_zero()	{
		cto_reset();
	}


	/***
	 * @brief: check
	 */
	bool cto_check(double diff){
		bool ret = false;
		GetLocalTime(&m_StEnd);
		return cto_check(&m_StBegin,&m_StEnd,diff);
	}
	
	bool cto_check(LPSYSTEMTIME start,double diff)
	{
		SYSTEMTIME tmp;
		GetLocalTime(&tmp);
		return cto_check(start,&tmp,diff);
	}
	
	bool cto_check(LPSYSTEMTIME start,LPSYSTEMTIME end,double diff)
	{
		bool ret = false;
		ULARGE_INTEGER m_fTimeBegin,m_fTimeEnd;
		double tmp = 0;
		/*
		printf("%02d:%02d:%02d:%04d -- %02d:%02d:%02d:%04d\n",
			start->wHour,start->wMinute,start->wSecond,start->wMilliseconds,
			end->wHour,end->wMinute,end->wSecond,end->wMilliseconds);
		*/
		SystemTimeToFileTime(start,(FILETIME*)&m_fTimeBegin);
		SystemTimeToFileTime(end,(FILETIME*)&m_fTimeEnd); 
		tmp = (double)(m_fTimeEnd.QuadPart-m_fTimeBegin.QuadPart); 
		tmp = (double)(tmp/10000);//��λ������

		return (tmp >= diff ? true : false);
	}


	/***
	 * @brief: set & get
	 */
	void cto_set_diff(double d){m_diff = (__int64)d;}
	__int64 cto_get_diff(){return m_diff;}


	bool cto_check(){return cto_check(m_diff);}
};

#endif   //HEADER_TIMEOUT_SHIQINGLIANG__