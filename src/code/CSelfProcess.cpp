#include <libs_version.h>
#include "../include/CSelfProcess.h"
#include <process.h>


//构造函数
CSelfProcess::CSelfProcess(void)
{
	m_ProExit = false;
	m_ProHand = INVALID_HANDLE_VALUE;
	m_runing = false;
}


// 析构函数
CSelfProcess::~CSelfProcess(void)
{
	CpStop();
}


// 启动
bool CSelfProcess::CpStart(unsigned (__stdcall * _StartAddress) (void *),void* Arglist,void*security, 
							 unsigned int _StackSize ,unsigned int _InitFlag, int Priority)
{
	m_ProHand = (HANDLE)_beginthreadex(security, _StackSize, _StartAddress, Arglist, 0, &m_proID);
	if (!m_ProHand)
	{
		return false;
	}

	if (!SetThreadPriority(m_ProHand, Priority))
	{
		return false;
	}
	m_runing = true;
	return true;
}

// 启动
bool CSelfProcess::CpStartEx(unsigned (__stdcall * _StartAddress) (void *),void* Arglist,void*security, 
						   unsigned int _StackSize ,unsigned int _InitFlag, int Priority)
{
	return (false == m_runing) ? CpStart(_StartAddress,Arglist,security,_StackSize,_InitFlag,Priority):true;
}



void CSelfProcess::CpStop()
{

	if (m_ProHand != INVALID_HANDLE_VALUE)
	{
		/** 通知线程退出 */ 
		m_ProExit = true;

		/** 等待线程退出 */ 
		com_sleep(10);

		/** 置线程句柄无效 */ 
		CloseHandle( m_ProHand );
		m_ProHand = INVALID_HANDLE_VALUE;
		m_runing = false;
	}
}