#ifndef HEADER_SOCKET_BASE_SHI_QINGLIANG
#define  HEADER_SOCKET_BASE_SHI_QINGLIANG


#include <iostream>
#include <string>

using namespace std;

#ifndef WIN32  
#include <sys/types.h>  
#include <sys/wait.h>  
#include <sys/socket.h>  
#include <arpa/inet.h>  
#include <netinet/in.h>  
#include <signal.h>  
#include <netdb.h>  
#include <unistd.h>  
#include <fcntl.h>  
#else  

#include <winsock2.h>  
#pragma comment(lib, "ws2_32.lib")

#endif  
// Linux  
#ifndef WIN32  
#define OP_SOCKET               int  
#define OP_SOCKADDR_IN          struct sockaddr_in  
#define OP_SOCKADDR             struct sockaddr  
#define OP_SOCKLEN_T            socklen_t  
// Windows  
#else  
#define OP_SOCKET               SOCKET  
#define OP_SOCKADDR_IN          SOCKADDR_IN  
#define OP_SOCKADDR             SOCKADDR  
#define OP_SOCKLEN_T            int FAR  
#endif  



class CBaseSocket
{
public:

	/***
	 * @breif: 构造函数、析构函数
	 * @param: null
	 * @return: null
	 */
	CBaseSocket(void);
	~CBaseSocket(void);

public:
	unsigned int	mVersion;		//socket版本
	OP_SOCKET		mSockHandle;	//socket句柄	
	unsigned int	mLocalPort;		//本地端口
	SOCKADDR_IN		mLocalSockAddr; //本地地址

	/***
	 * @breif: 设置socket版本
	 * @param version: 版本
	 * @return: null
	 */
	void cbs_SetVersion(unsigned int version) {	mVersion = version;	};

	/***
	 * @brief: 初始会版本
	 * @param version: 版本号
	 * @return null
	 */
	void cbs_InitVersion(unsigned int version = 1);


	/***
	 * @brief: 获取源地址
	 * @return: 0 for failed, 1 for OK.
	 */
	bool cbs_FormatStr(SOCKADDR_IN* addr,string& ip, unsigned int& port);

	/***
	 * @brief: 初始化socket地址信息
	 * @param nat:
	 * @param soca_in
	 */
	 bool cbs_InitSockAddr(const string ip, const unsigned int port, SOCKADDR_IN* soca_in);


	/***
	 * 本地端口
	 */
	void cbs_LocalPortSet(unsigned int port) {mLocalPort = port;};

	/***
	 * 获取本地IP地址
	 */
	string cbs_GetLocalAddr(int index = 0);
	//int cbs_GetLocalAddr(IP_Addr_t* nat);
	
	/***
	 * 获取本地名称
	 */
	string cbs_GetLocalName();


	/***
	 * 初始化本地socket信息
	 * @param localport:
	 */
	char cbs_InitLoaclSocket(bool tcp = true, unsigned int localport = 0);

	/***
	 *
	 */
	virtual void run(){};

};

#endif //HEADER_SOCKET_BASE_SHI_QINGLIANG