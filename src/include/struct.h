#ifndef HADER_STRUCT_SHIQINGLINAG__
#define HADER_STRUCT_SHIQINGLINAG__
#include <iostream>
#include <string>
#include <list>

#include <CXML.h>
#include <entity/CBrickEntity.h>
#include <entity/CSiteEntity.h>
#include <entity/CDeviceEntity.h>

using namespace std;


class CSTRUCT
{
public:

	/***
	 * @brief: 构造、析构
	 */
	CSTRUCT();
	~CSTRUCT();


	/***
	 * @brief: 销毁
	 */
	static void Destroy(CSiteEntity *s);

	/***
	 * @brief: 开始工作
	 */
	static bool DoWork(CSiteEntity *site);

	/***
	 * @brief:获取配置文件
	 */
	static bool ParseConfig(list<CSiteEntity>& filelist);
};


#endif