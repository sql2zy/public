#ifndef HEADER_HTTPCONFIG_SHIQINGLIANG__
#define HEADER_HTTPCONFIG_SHIQINGLIANG__

#include <iostream>
#include <string>
#include <CSelfProcess.h>
#include <HttpClient.h>
#include <Lock.hpp>

#include <vector>
using namespace std;

typedef void(*HTTPCALLBACK)(string);

class CHttpNode: public CSelfProcess, public CHttpClient{
public:

	/***
	 * @brief: structrue & destructure
	 */
	CHttpNode(void);
	CHttpNode(string url, int interval, HTTPCALLBACK hcb);
	~CHttpNode(void);

protected:
	string	mUrl;
	int		mInterval;
	HTTPCALLBACK mHttpCallback;

	/***
	 * @brief: main thread
	 */
	void run();

public:
	
	/***
	 * @brief: 
	 */
	static unsigned WINAPI router(void* lpvoid);

	/***
	 * @brief: �˳�
	 */
	void Stop();

	/***
	 * @brief: ��ʼ
	 */
	bool Start(string url = "", int interval = 0,HTTPCALLBACK hcb = NULL);

	/***
	 * @brief: operator ==
	 */
	bool operator == (const CHttpNode& hn);

	bool Check(string str);

	void Update(HTTPCALLBACK hcb, int interval){mHttpCallback = hcb; mInterval = interval;};
};

/************************************************************************/
/*                                                                      */
/************************************************************************/
class CHttpConfig
{
public:

	/***
	 * @brief: structrue & destructure
	 */
	CHttpConfig(void);
	~CHttpConfig(void);

protected:

	CMyLock mLock;
	vector<CHttpNode*> mVector;

	/***
	 * @brief: find
	 */
	CHttpNode* FindNode(string url);

	/***
	 * @brief: update
	 */
	void UpdateNode(CHttpNode* node, HTTPCALLBACK cb, int interval);

public:

	/***
	 * @brief: add client
	 * @param	url:
	 *			cb:
	 *			interval:	(Unit:milliscond)
	 */
	bool AddClient(string url, HTTPCALLBACK cb, int interval = 10*100);
	bool DeleteClient(string url);

	/***
	 * @brief: start web server
	 */
	bool AddServer(string url, int port, HTTPCALLBACK cb);
};

#endif //HEADER_HTTPCONFIG_SHIQINGLIANG__