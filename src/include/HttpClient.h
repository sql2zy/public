#ifndef HEADER_HTTP_SHIQINGLIANG__
#define HEADER_HTTP_SHIQINGLIANG__
#include "../include/common.h"

class CHttpClient
{
public:
	/** 构造、析构 **/
	CHttpClient(void);
	~CHttpClient(void);

	/*
	//禁用拷贝、赋值函数
	CHttpClient(const CHttpClient&);
	CHttpClient& operator=(const CHttpClient&);
	*/

public:

	/***
	 * @brief: 发送数据
	 * @param data: 待发送数据
	 * @param len: 待发送数据长度
	 * @return 发送成功true, 失败false
	 */
	bool http_SendData(char* data, unsigned len);
	bool http_SendData(string message);
	
public:
	/**
	* @brief HTTP POST请求
	* @param strUrl 输入参数,请求的Url地址,如:http://www.baidu.com
	* @param strPost 输入参数,使用如下格式para1=val1&para2=val2&…
	* @param strResponse 输出参数,返回的内容
	* @return 返回是否Post成功
	*/
	int Post(const std::string & strUrl, const std::string & strPost, std::string & strResponse);

	/**
	* @brief HTTP GET请求
	* @param strUrl 输入参数,请求的Url地址,如:http://www.baidu.com
	* @param strResponse 输出参数,返回的内容
	* @return 返回是否Post成功
	*/
	int Get(const std::string & strUrl, std::string & strResponse,int& httpflag);


	/***
	 * @brief: HTTP PUT请求
	 */
	int Put(const std::string & strUrl, const std::string & strPut, std::string & strResponse);

	/**
	* @brief HTTPS POST请求,无证书版本
	* @param strUrl 输入参数,请求的Url地址,如:https://www.alipay.com
	* @param strPost 输入参数,使用如下格式para1=val1&para2=val2&…
	* @param strResponse 输出参数,返回的内容
	* @param pCaPath 输入参数,为CA证书的路径.如果输入为NULL,则不验证服务器端证书的有效性.
	* @return 返回是否Post成功
	*/
	int Posts(const std::string & strUrl, const std::string & strPost, std::string & strResponse, const char * pCaPath = NULL);

	/**
	* @brief HTTPS GET请求,无证书版本
	* @param strUrl 输入参数,请求的Url地址,如:https://www.alipay.com
	* @param strResponse 输出参数,返回的内容
	* @param pCaPath 输入参数,为CA证书的路径.如果输入为NULL,则不验证服务器端证书的有效性.
	* @return 返回是否Post成功
	*/
	int Gets(const std::string & strUrl, std::string & strResponse, const char * pCaPath = NULL);

public:
	void SetDebug(bool bDebug) {m_bDebug = bDebug;};

private:
	bool m_bDebug;
};

#endif //HEADER_HTTP_SHIQINGLIANG__