#ifndef HEADER_RESOURCECONFIG_SHIQINGLIANG__
#define HEADER_RESOURCECONFIG_SHIQINGLIANG__

#include <Entity.h>

class CResourceConfig
{
private:

	/***
	 * @brief: structrue & destructrue
	 */
	CResourceConfig(void);
	~CResourceConfig(void);

	CResourceConfig(const CResourceConfig&);
	CResourceConfig& operator=(const CResourceConfig&);


	list<CSiteEntity> mConfigList;	//列表

private:

	/***
	 * @brief:
	 */
	bool rc_StartClusters(CSiteEntity* site);
	bool rc_StartDevices(CClusterEntity* cluster);
	bool rc_Start(CDeviceEntity* device);

public:

	/***
	 * @brief: instance
	 */
	static CResourceConfig* GetInstance(){
		static CResourceConfig mResourceConfig;
		return &mResourceConfig;
	}

	/***
	 * @brief: parse config file
	 */
	bool MapConfig();

	/***
	 * @brief: start
	 */
	bool rc_Start();

	/***
	 * @brief: 获取site数目
	 */
	unsigned int rc_GetCount(){return mConfigList.size();};

	/***
	 * @brief: 获取site对象
	 */
	CSiteEntity* rc_GetSite(unsigned int index);

	/***
	 * @brief: 获取device对象
	 */
	CDeviceEntity* rc_GetDevice(unsigned int sid, unsigned int cid, unsigned int did);
	CDeviceEntity* rc_GetDevice(CIdenEntity* ie);
	
};

#endif //HEADER_RESOURCECONFIG_SHIQINGLIANG__