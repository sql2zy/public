#ifndef HEADER_CUDP_SHIQINGLIANG__
#define HEADER_CUDP_SHIQINGLIANG__

#include <iostream>
#include <string>
#include <CSelfProcess.h>
#include <BaseNet.h>
#include <Lock.hpp>

using namespace std;


// data, data_len, fromip, fromport, lpvoid, attr
typedef void(*UDP_CALLBACK)(char*, unsigned int, string ,unsigned int,unsigned int, void*);

class Cudp: public CBaseSocket, public CSelfProcess
{
public:
	Cudp(void);
	Cudp(unsigned int port, UDP_CALLBACK recv_cb = NULL, UDP_CALLBACK send_cb = NULL, unsigned int lpvoid = 0, void* addtion = NULL);
	~Cudp(void);

protected:
	UDP_CALLBACK mRecvCallback;
	UDP_CALLBACK mSendFaildCallback;
	CMyLock		 mLock;
	unsigned int mLpvoid;
	void*		 mAddition;

public:

	/***
	 * @brief: ���ûص�����
	 */
	void Assgin(UDP_CALLBACK recv_cb, UDP_CALLBACK send_cb,unsigned int lpvoid, void * addtion);

	/***
	 * @brief: start
	 */
	bool Start(unsigned int port = 0);

	/***
	 * @brief: Send data
	 */
	int SendData(string toip, unsigned int toport,char* pdata, unsigned int len);


	void run();
};

#endif //HEADER_CUDP_SHIQINGLIANG__