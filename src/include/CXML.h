#ifndef HEADER_CMXL_SHIQINGLIANG__
#define HEADER_CMXL_SHIQINGLIANG__
#include <iostream>
#include <string>
#include <xml/tinyxml.h>
#include <string2type.hpp>

using namespace std;

class CXML: public Cstring2type
{
public:
	CXML(void);
	~CXML(void); 
	bool ParseXmlFile(const char* xmlFile); 
	void Clear(); 
	void SaveFile(const char* file); 
	//////////////////////////////////////////////////////////////////////////
	TiXmlElement * GetRootElement();
	TiXmlElement * GetChildElement(TiXmlElement * pParentElement,const char * title);
	TiXmlElement * GetFirstChildElement(TiXmlElement * pParentElement); 
	TiXmlElement * GetNextChildElement(TiXmlElement * pElement); 
	//////////////////////////////////////////////////////////////////////////
	TiXmlElement * FindFirstElement(const char * title);//�ݹ����
	//////////////////////////////////////////////////////////////////////////
	std::string GetElementValue(TiXmlElement * pElement);
	std::string GetElementAttributeValue(TiXmlElement* Element,const char* AttributeName);
	std::string GetChildElementValue(TiXmlElement * pParentElement,const char * title);
	std::string GetChildElementAttributeValue(TiXmlElement * pParentElement,const char *title,const char* AttributeName);

	//sql add
	template<typename T>
	T GetElementValue(TiXmlElement * pElement,T var){
		string ret = GetElementValue(pElement);
		return string2type(ret,var);
	}

	template<typename T>
	T GetElementAttributeValue(TiXmlElement* Element,const char* AttributeName,T var)
	{
		string ret = GetElementAttributeValue(Element,AttributeName);
		return string2type(ret,var);
	}

	template<typename T>
	T GetChildElementValue(TiXmlElement * pParentElement,const char * title,T var)
	{
		string ret = GetChildElementValue(pParentElement,title);
		return string2type(ret,var);
	}


	template<typename T>
	T GetChildElementAttributeValue(TiXmlElement * pParentElement,const char *title,const char* AttributeName,T var)
	{
		string ret = GetChildElementAttributeValue(pParentElement,title,AttributeName);
		return string2type(ret,var);
	}

	//////////////////////////////////////////////////////////////////////////
	TiXmlElement* AddXmlRootElement(const char* title);
	TiXmlElement* AddXmlChildElement(TiXmlElement* pPareElement,const char* title,const char * value = "\0"); 
	void AddXmlAttribute(TiXmlElement* pElement,const char* name,const char* value); 
	void AddXmlDeclaration(const char* vesion="1.0",const char* encoding="gb2312",const char* standalone=""); 
	void AddElementValue(TiXmlElement * pElement,const char* value); 
	void AddXmlComment(TiXmlElement* pElement,const char* Comment); 
	//////////////////////////////////////////////////////////////////////////
	bool ReplaceElementValue(TiXmlElement * pElement,const char * newValue);
	bool ReplaceElementAttribute(TiXmlElement* pElement,const char * name,const char *newValue);
	//////////////////////////////////////////////////////////////////////////

	/***
	 * @brief: Accept
	 */
	bool Accept(TiXmlPrinter* Printer){return m_xml.Accept(Printer);}


	/***
	 * @brief: ����
	 */
	bool Parse(string message);

private:
	TiXmlElement * FindFirstElement(TiXmlElement* pcrElement,const char * title);//�ݹ����

private: 
	TiXmlDocument m_xml; 
};

#endif