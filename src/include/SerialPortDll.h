#ifndef HEADER_SERIALPORTDLL_SHIQINGLIANG__
#define HEADER_SERIALPORTDLL_SHIQINGLIANG__

#ifdef SERIALPORTDLL_EXPORTS
#define SERIALPORTDLL_API __declspec(dllexport)
#else
#define SERIALPORTDLL_API __declspec(dllimport)
#endif
#include <Windows.h>


#ifdef __cplusplus
extern "C"{
#endif

	typedef struct _com_state com_State;
	/***
	 * @brief: 
	 */
	typedef void(*com_getdata_cb)(char*, unsigned int, unsigned int, void*);

	///////////////////////////////// state manipulation /////////////////////////////////////////

	/***
	 * @brief: molloc handle
	 */
	SERIALPORTDLL_API com_State* com_NewState(); 

	/***
	 * @brief: assgin get data callback
	 */
	SERIALPORTDLL_API int com_Initialize(
		com_State*		com,
		unsigned int	portNo,		//串口后
		unsigned int	baud,		//波特率
		char			parity,		//校验
		unsigned int	databits,	//数据位
		unsigned int	stopbits,	//停止位
		com_getdata_cb	cb,			//回调函数
		void*			addition	//附加消息
		);

	/***
	 * @brief: close the handle
	 */
	SERIALPORTDLL_API void com_Close(com_State* com);

	/***
	 * @brief: send data
	 */
	SERIALPORTDLL_API BOOL com_Send(com_State* com, char* data, unsigned int len);

	/***
	 * @brief: get port number
	 */
	SERIALPORTDLL_API unsigned int com_GetNo(com_State* com);

#ifdef __cplusplus
};
#endif

#endif //HEADER_SERIALPORTDLL_SHIQINGLIANG__