#ifndef HEADER_STRING2TYPE_SHIQINGLIANG__
#define HEADER_STRING2TYPE_SHIQINGLIANG__

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

class Cstring2type
{
public:
	Cstring2type(void){};
	~Cstring2type(void){};


	int string2int(string str){
		int d = 0;
		return string2type(str,d);
	}

	float string2float(string str){
		float d = 0.0L;
		return string2type(str,d);
	}

	template<typename T>
	T string2type(string str, T ret){
		stringstream ss;
		ss.str("");
		ss<<str;
		ss>>ret;
		return ret;
	}

	template<typename T>
	string type2string(T var){

		stringstream ss;
		ss.str("");
		ss<<var;
		return ss.str();
	}

};

#endif