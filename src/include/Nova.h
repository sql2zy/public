#ifndef HEADER_NOVA_SHIQINGLIANG_20141021

#define HEADER_NOVA_SHIQINGLIANG_20141021


//系统工作模式
#define		BMS_MODE_UPS_MAIN				0x02	//UPS主机工作模式
#define		BMS_MODE_UPS_ASSISTANT			0x03	//UPS从机工作模式

//site status
#define SITE_STATUS_NORMAL					0x01		//外电正常
#define SITE_STATUS_UPS_CHARGING			0x02		//UPS供电
#define SITE_STATUS_PARTIAL_OUTAGE			0x03		//负载部分断电
#define SITE_STATUS_COMPLATE_OUTAGE			0x04		//负载全部断电

//device status
#define DEVICE_STATUS_IDLE					0x00		//空闲
#define DEVICE_STATUS_DISCHARGING			0x01		//放电
#define DEVICE_STATUS_CHARGING				0x02		//充电
#define DEVICE_STATUS_WARNING				0x03		//告警

//brick status
#define UNIT_STATUS_IDLE					0x00		//空闲
#define UNIT_STATUS_DISCHARGING				0x01		//放电
#define UNIT_STATUS_CHARGING				0x02		//充电
#define UNIT_STATUS_ALERT					0x03		//报警
#define UNIT_STATUS_SUPERCHARGE				0x04		//超级电容充电
#define UNIT_STATUS_WAITING_TO_STOP			0x05		//停机前等待
#define UNIT_STATUS_STOP					0x06		//停机
#define UNIT_STATUS_ERROR					0x07		//错误

#define _NOVA_CMD_DEFINE(v,v1,v2,v3) ((v<<8) | (v1<<2) | (v2<<1) | (v3<<0))
#define _NOVA_CMD_ID(v) ((v & 0xFFFF00) >>8)

#define _NOVA_ENABLE_BS(v) v & 0x04 ? true : false
#define _NOVA_ENABLE_BU(v) v & 0x02 ? true : false
#define _NOVA_ENABLE_BM(v) v & 0x01 ? true : false

//common command
/************************************************************************
	_NOVA_CMD_DEFINE(v,x,y,z)
	v: the command id
	x: used for device (0:no 1:yes)
	y: used for unit 
	z: used for module
 ************************************************************************/
// common command id
#define CMD_RESP							_NOVA_CMD_DEFINE(0x0002,1,1,1)		//命令响应
#define CMD_PING							_NOVA_CMD_DEFINE(0x0003,1,1,1)	
#define CMD_SET_BOARD_ADD					_NOVA_CMD_DEFINE(0x0004,1,1,1)	
#define CMD_GET_BOARD_ADD					_NOVA_CMD_DEFINE(0x0005,1,1,1)	
#define CMD_SET_ETH_IP						_NOVA_CMD_DEFINE(0x0006,1,1,0)	
#define CMD_GET_ETH_IP						_NOVA_CMD_DEFINE(0x0007,1,1,0)
#define CMD_GET_VERSION						_NOVA_CMD_DEFINE(0x0008,1,1,1)	
#define CMD_UPDATE_FW						_NOVA_CMD_DEFINE(0x0009,1,1,0)	
#define CMD_UPDATE_FPGA						_NOVA_CMD_DEFINE(0x000A,1,1,0)

//system data
#define CMD_GET_BS_INFO						_NOVA_CMD_DEFINE(0x0020,1,0,0)	
#define CMD_GET_BU_INFO						_NOVA_CMD_DEFINE(0x0021,1,1,0)	
#define CMD_GET_BM_INFO						_NOVA_CMD_DEFINE(0x0022,1,1,1)

//runing status
#define CMD_SET_BS_MODE						_NOVA_CMD_DEFINE(0x0030,1,0,0)	
#define CMD_GET_BS_MODE						_NOVA_CMD_DEFINE(0x0031,1,0,0)

#define CMD_SET_BS_RUNSTATE					_NOVA_CMD_DEFINE(0x0040,1,0,0)	
#define CMD_GET_BS_RUNSTATE					_NOVA_CMD_DEFINE(0x0041,1,0,0)
#define CMD_SET_BU_RUNSTATE					_NOVA_CMD_DEFINE(0x0042,1,1,0)	
#define CMD_GET_BU_RUNSTATE					_NOVA_CMD_DEFINE(0x0043,1,1,0)
#define CMD_SET_BM_RUNSTATE					_NOVA_CMD_DEFINE(0x0044,1,1,1)	
#define CMD_GET_BM_RUNSTATE					_NOVA_CMD_DEFINE(0x0045,1,1,1)

#define CMD_SET_BM_CHARGE_CFG				_NOVA_CMD_DEFINE(0x0050,1,1,1)	
#define CMD_GET_BM_CHARGE_CFG				_NOVA_CMD_DEFINE(0x0051,1,1,1)
#define CMD_SET_BM_DISCHARGE_CFG			_NOVA_CMD_DEFINE(0x0052,1,1,1)	
#define CMD_GET_BM_DISCHARGE_CFG			_NOVA_CMD_DEFINE(0x0053,1,1,1)

//parameter setting
#define CMD_SET_BS_CFGFILE					_NOVA_CMD_DEFINE(0x0080,1,0,0)	
#define CMD_GET_BS_CFGFILE					_NOVA_CMD_DEFINE(0x0081,1,0,0)
#define CMD_SET_BU_CFGFILE					_NOVA_CMD_DEFINE(0x0082,1,1,0)	
#define CMD_GET_BU_CFGFILE					_NOVA_CMD_DEFINE(0x0083,1,1,0)
#define CMD_SET_BM_CFGFILE					_NOVA_CMD_DEFINE(0x0084,1,1,1)	
#define CMD_GET_BM_CFGFILE					_NOVA_CMD_DEFINE(0x0085,1,1,1)

#define CMD_SET_BAT_SOC						_NOVA_CMD_DEFINE(0x0090,1,1,1)
#define CMD_GET_BAT_SOC						_NOVA_CMD_DEFINE(0x0091,1,1,1)
#define CMD_SET_BAT_SOH						_NOVA_CMD_DEFINE(0x0092,1,1,1)
#define CMD_GET_BAT_SOH						_NOVA_CMD_DEFINE(0x0093,1,1,1)
#define CMD_SET_BAT_MASK					_NOVA_CMD_DEFINE(0x0094,1,1,1)
#define CMD_GET_BAT_MASK					_NOVA_CMD_DEFINE(0x0095,1,1,1)

//others
#define CMD_SET_DEBUGLOG_XX					_NOVA_CMD_DEFINE(0x00C0,1,1,1)

//////////////////////////////////////////////////////////////////////////
//电池状态
#define BAT_STATUS_NORMAL			0	//正常
#define BAT_STATUS_FULL				1	//满电压
#define BAT_STATUS_EMPTY			2	//低电压
#define BAT_STATUS_ERROR			3	//电池故障

//////////////////////////////////////////////////////////////////////////
// UPS系统状态
#define UPS_SYSTEM_STATE_IDLE				0x00		//空闲
#define UPS_SYSTEM_STATE_DISCHARGING		0x01		//放电
#define UPS_SYSTEM_STATE_CHARGING			0x02		//充电
#define UPS_SYSTEM_STATE_WARNING			0x03		//告警
#define UPS_SYSTEM_STATE_SUPER_CHARGING		0x04		//超级电容充电
#define UPS_SYSTEM_STATE_UPS_WAIT_DOWN		0x05		//停机前等待
#define UPS_SYSTEM_STATE_UPS_DOWN			0x06		//停机状态

//


//soc数值
struct __value_soc_t
{
	uint fcc;	//峰值
	uint rm;	//内存值
	uint soc;
};
typedef struct __value_soc_t val_soc_t;


#endif //HEADER_NOVA_SHIQINGLIANG_20141021