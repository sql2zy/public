#ifndef HEADER_COM_BUF_SHIQINGLIANG
#define HEADER_COM_BUF_SHIQINGLIANG
#include "common.h"

#define BUFFER_MAX 100

#define _COMBUF_(var) (var)""

class CComBuffer
{
public:

	/***
	 * @brief: structure & destructure
	 */
	CComBuffer(void);
	~CComBuffer(void);

	/***
	 * @brief: emp
	 */
	void Zero();

	unsigned int mLeng;
	unsigned char mBody[BUFFER_MAX];

	

	/***
	 * @brief: emp
	 */
	template<typename T>
	T* ToString(T*){

		T* ret = NULL;
		if(mLeng > 0)ret = (T*)&(mBody[0]);
		return ret;
	}

	/***
	 * @brief: add char
	 */
	unsigned int Append(unsigned char c);

	
	/***
	 * @brief: add char*
	 */
	unsigned int Append(unsigned char* str, unsigned int len);

	/***
	 * @brief: add va_list
	 */
	unsigned int Append2(int cut,...);

	/***
	 * @brief: Get length
	 */
	unsigned int com_buf_length(){return mLeng;};

};

#pragma region PAYLOAD

class CPayload: public CComBuffer
{
public:

	/***
	 * @brief: 
	 */
	CPayload();
	~CPayload();

	unsigned short mCommandID;
	unsigned int	mPayload_len;

	/***
	 * @brief: clear
	 */
	void Clear(){
		mCommandID = 0;
		__super::Zero();
	};

	void AssignID(unsigned char cmdid){mCommandID = cmdid;};

	/***
	 * @brief: get the command id
	 */
	unsigned short GetCmdID(){return mCommandID;};

	/***
	 * @brief: 
	 */
	unsigned int Payload2String(CComBuffer& cb);


};

#pragma endregion PAYLOAD


class ActionBuffer : public CPayload
{
public:
	/***
	 * @brief: structrue & destructure
	 */
	ActionBuffer(void);
	ActionBuffer(unsigned char srcaddr, unsigned char bsaddr,unsigned short cmdid, unsigned char buaddr = 0xFF, unsigned char bmaddr = 0xFF, 
		unsigned char reservation = 0);
	ActionBuffer(unsigned char srcaddr[4], unsigned char bsaddr,unsigned short cmdid, unsigned char buaddr = 0xFF, unsigned char bmaddr = 0xFF, 
		unsigned char reservation = 0);
	~ActionBuffer(void);

	
	
	/***
	 * @brief: to pointer
	 */
	template<typename T>
	T* c_str(T* ){

		mTempContainer.Zero();
		T* ret = NULL;
		CComBuffer payload;
		mTempContainer.Append(mSrcAddr[0]);		//the source addr
		mTempContainer.Append2(4,mRevervation,mBSAddr,mBUAddr,mBMAddr);	//the reservation 

		int len  = Payload2String(payload);
		mTempContainer.Append2(2,len & 0xFF00,len & 0x00FF);									//payload length
		mTempContainer.Append(payload.ToString(_COMBUF_(unsigned char*)),payload.com_buf_length());	// payload data

		unsigned char dst[3] = {0}; 
		get_crc16(payload.ToString(_COMBUF_(unsigned char*)),payload.com_buf_length(),dst);
		mTempContainer.Append(dst[0]);
		mTempContainer.Append(dst[1]);
		return mTempContainer.ToString(_COMBUF_(T*));
	}

	unsigned char mSrcAddr[4];	//来源地址
	unsigned char mRevervation;	//保留位
	unsigned char mBSAddr;		//设备地址
	unsigned char mBUAddr;		//单元地址
	unsigned char mBMAddr;		//模块地址
	unsigned char mCRC[2];		//校验位

	unsigned int  mDatalen;		//data length

	int get_crc16 (unsigned char *bufData, unsigned int buflen, unsigned char *pcrc);

	/***
	 * @brief: 请在c_str之后获取
	 */
	unsigned int action_length();

	/***
	 * @brief: assign paramers
	 */
	void Assgin(unsigned char srcaddr, unsigned char bsaddr, unsigned char cmdid, unsigned char buaddr = 0xFF, unsigned char bmaddr = 0xFF, 
		unsigned char reservation = 0);
	void Assgin(unsigned char srcaddr[4], unsigned char bsaddr, unsigned char cmdid,unsigned char buaddr = 0xFF, unsigned char bmaddr = 0xFF, 
		unsigned char reservation = 0);

	/***
	 * @brief: clear
	 */
	void Clear();

	/***
	 * @brief: 解析
	 */
	bool Parse(unsigned char* data, unsigned int len);

	DWORD	GetDword(int pos);
	WORD	GetWord(int pos);
	int		GetByte(int pos);


protected:
	unsigned char aLength;		//长度

	CComBuffer mTempContainer;
};


class CStringBuffer:public CComBuffer
{
public:
	CStringBuffer(void);
	CStringBuffer(string str);
	~CStringBuffer(void);

	void convert(string str);
};
#endif //HEADER_STD_SHIQINGLIANG