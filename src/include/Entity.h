#ifndef HEADER_ENTITY_SHIQINGLIANG__
#define HEADER_ENTITY_SHIQINGLIANG__

#include <entity/CActionEntity.h>
#include <entity/CAddressEntity.hpp>
#include <entity/CBatteryEntity.h>
#include <entity/CBrickEntity.h>
#include <entity/CClusterEntity.h>
#include <entity/CComIDEntity.hpp>
#include <entity/CDeviceEntity.h>
#include <entity/CIdentityEntity.hpp>
#include <entity/CParamsEntity.h>
#include <entity/CSiteEntity.h>
#include <entity/CUnitCtrlEntity.h>
#include <entity/HttpDataEntity.h>

#endif //HEADER_ENTITY_SHIQINGLIANG__