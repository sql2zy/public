#ifndef HEADER_CEVENTCENTRA_SHIQINIGLIANG__
#define HEADER_CEVENTCENTRA_SHIQINIGLIANG__

#include <Windows.h>
#include <string>
using namespace std;

class CEventInterface
{
public: 
	CEventInterface(void){};
	~CEventInterface(void) {};

	/***
	 * @brief:
	 */
	virtual void do_work(unsigned int EventID, WPARAM wparam, LPARAM lparam, LPVOID data){};
};


//////////////////////////////////////////////////////////////////////////
class CEventNode
{
public:
	CEventNode();
	CEventNode(unsigned int EventID, WPARAM wparam, LPARAM lparam, LPVOID data, string key = "");
	~CEventNode();

	string mKey;
	unsigned int mID;
	WPARAM mWparam;
	LPARAM mLparam;
	LPVOID mLPVoid;

	void Zero();
	void Fill(unsigned int EventID, WPARAM wparam, LPARAM lparam, LPVOID data, string key = "");

	CEventNode& operator=(const CEventNode& from);

	void Call(CEventInterface* inter);
};

//////////////////////////////////////////////////////////////////////////
//
class CEventCentra
{
private:
	/***
	 * @brief: structrue & destructrue
	 */
	CEventCentra(void);
	~CEventCentra(void);

	CEventCentra(const CEventCentra&);
	CEventCentra& operator=(const CEventCentra&);


public:

	/***
	 * @brief: interface
	 */
	static CEventCentra* GetInstance(){

		static CEventCentra mEventCentra;
		return &mEventCentra;
	}

	/***
	 * @brief: add
	 */
	bool ec_Add(string key,CEventInterface* ei);

	/***
	 * @brief: delete
	 */
	bool ec_Del(string key);

	/***
	 * @brief: Event
	 */
	void ec_Event(unsigned int EventID, WPARAM wparam = 0, LPARAM lparam = 0,  LPVOID lpVoid = NULL);
	bool ec_EventEx(string key,unsigned int EventID, WPARAM wparam = 0, LPARAM lparam = 0,  LPVOID lpVoid = NULL);

protected:

	HANDLE mHandle;
	bool mExit;
	static unsigned WINAPI EventRouter(LPVOID lpvoid);

	void EventRouter();
};

#endif //HEADER_CEVENTCENTRA_SHIQINIGLIANG__