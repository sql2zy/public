#ifndef HEADER_HTTPDATAENTITY_SHIQINGLIANG__
#define HEADER_HTTPDATAENTITY_SHIQINGLIANG__

#include <iostream>
#include <string>
using namespace std;

#pragma region CHttpExceptionEntity

class CHttpExceptionEntity
{
public:
	CHttpExceptionEntity(void);
	CHttpExceptionEntity(string type, int code, string message);
	~CHttpExceptionEntity(void);

	string mType;
	int mCode;
	string mMessage;

	/***
	 * 
	 */
	void Zero();

	/***
	 * 
	 */
	CHttpExceptionEntity& operator = (const CHttpExceptionEntity& from);

};

#pragma endregion CHttpExceptionEntity


#pragma  region CHttpComDataEntity

class CHttpComDataEntity
{
public:
	/***
	 * @brief: strucutre/destructure
	 */
	CHttpComDataEntity(void);
	~CHttpComDataEntity(void);

	/***
	 * @brief: initlization
	 */
	void Zero();

	int mUID;				//ID
	string mName;			//����
	string mDescribtion;	//����
	bool mActive;
	string mVersion;
	string mVersionUpdatedAt;	

	CHttpExceptionEntity mException;
	int mHttpCode;

public:

	/***
	 * @brief:  Copy
	 */
	CHttpComDataEntity& operator = (const CHttpComDataEntity& from);
};

#pragma  endregion CHttpComDataEntity


#pragma  region CHttpDeviceEntity

class CHttpDeviceEntity
{
public:
	/***
	 * @brief: strucutre/destructure
	 */
	CHttpDeviceEntity(void);
	~CHttpDeviceEntity(void);

	/***
	* @brief: initlization
	*/
	void Zero();
	
	int mBelongSiteID;
	int mBelongClusterID;
	int mUID;				//ID
	int mNumofBricks;
	int mFunc;
	string mType;
	string mName;			//����
	bool mActive;
	string mVersion;
	string mVersionUpdatedAt;

	CHttpExceptionEntity mException;
	int mHttpCode;


public:

	/***
	 * @brief:  Copy
	 */
	CHttpDeviceEntity& operator = (const CHttpDeviceEntity& from);
};

#pragma  endregion CHttpDeviceEntity
#endif