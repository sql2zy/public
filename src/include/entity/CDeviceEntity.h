#ifndef HEADER_DEVICEENTITY_SHIQINGLIANG__
#define HEADER_DEVICEENTITY_SHIQINGLIANG__

#include <iostream>
#include <string>
#include <list>
#include <entity/CBrickEntity.h>
#include <entity/CComIDEntity.hpp>
#include <entity/CAddressEntity.hpp>
using namespace std;

typedef enum __DEVICE_CLASSIFY_E
{
	DC_UPS = 0,
	DC_CHARGING_STATION,
	DC_ENERGY_STORAGE

}device_classify_e;

#pragma region HTTPENTITY

class CHttpEntity
{
public:

	CHttpEntity(void);
	~CHttpEntity(void);

	void Zero();

	string mUrl;
	unsigned int mInterval;

	CHttpEntity& operator = (const CHttpEntity& from);

	void SetUrl(string url){mUrl.clear(); mUrl.append(url);};
	void SetInterval(unsigned int interval){mInterval = interval;};
};

#pragma endregion HTTPENTITY

#pragma region _HANDLE_

class CHandleEntity
{
public:
	CHandleEntity(){mHandle = 0;};
	~CHandleEntity(){};

	unsigned int mHandle;
	unsigned int GetHandle(){return mHandle;};
	void SetHandle(unsigned int handle){mHandle = handle;};
};

#pragma endregion _HANDLE_

class CDeviceEntity: public CComIDEntity, public CAddressEntity,public CHandleEntity
{
public:
	/***
	 * @brief: structrue/destructure
	 */
	CDeviceEntity(void);
	~CDeviceEntity(void);

	char mOutPut;			//
	char mClassify;			//
	string mAddr;

	int mType;
	int mFunc;
	int mNum;
	int mUPSmap;

	int mState;		//状态 0: 空闲,1:放电,2:充电,3:告警
	int mCurrent;	//电流
	int mVoltage;	//电压
	unsigned char	mTemperature;		//温度
	__int64			mDuration;			//停电时，以目前平均电流设备还能供电多长时间(单位：minute)
	__int64			mSOC;
	string			mWarnings;			//警告信息

	void* mPvoid;

	/***
	 * @brief: 初始化数据
	 */
	void Zero();

	std::list<CBrickEntity*> mBrickList;

	/***
	 * @brief: 添加电池
	 */
	bool AddBrick(CBrickEntity* brick);

	/***
	 * @brief: 获取
	 */
	CBrickEntity* GetBick(unsigned int uid);

	/***
	 * @brief: 获取brick数量
	 */
	unsigned int GetBricksCount();

	void SetVoid(void* v){
		mPvoid = v;
	}

	void* GetVoid(){return mPvoid;};

protected:

private:

};



#endif