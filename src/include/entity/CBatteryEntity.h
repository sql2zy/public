#ifndef HEADER_CBATTERYENTITY_SHIQINGLIANG__
#define HEADER_CBATTERYENTITY_SHIQINGLIANG__
#include <iostream>
using namespace std;
#include <list>
#include <Lock.hpp>

//////////////////////////////////////////////////////////////////////////
// things about battery
class CBatteryEntity
{
public:

	/***
	 * @brief: structure、destructure
	 */
	CBatteryEntity(void); 
	~CBatteryEntity(void);

public:

	unsigned int mUID;
	char mState;			//电池状态
	bool mPartake;			//是否参与
	char mBypassSwitch;		//旁路开关
	char mSwitch;			//开关
	unsigned int mCurrent;	//电流(单位:mA)
	unsigned int mVoltage;	//电压(单位:mV)
	bool mMask;				//是否屏蔽
	unsigned short mCapacity;	//电池容量

	unsigned char mSOC;
	unsigned char mSOH;
	float mSize;			//尺寸
	string	mMaterial;		//原料
	string mPackaging;		//包装
	string mShape;			//形状

	unsigned char mTemprature;	//温度
	string mName;
	
	unsigned short MaxChargVoltage;
	unsigned short MaxChargCurrent;
	
	unsigned short CutofVoltage;
	unsigned short CutofCurrent;

	/***
	 * @brief: initialize
	 */
	void Zero();

	/***
	 * @brief: opretor =
	 */
	CBatteryEntity& operator = (CBatteryEntity& u);

	void SetName(string name);
	void SetName(int name);

};


//////////////////////////////////////////////////////////////////////////
// things about module

class CModuleEntity
{
public:

	/***
	* @brief: structure、destructure
	*/
	CModuleEntity(void);
	CModuleEntity(unsigned int UID);
	~CModuleEntity(void);

	/***
	 * @brief: initialize
	 */
	void Zero();

private:

	CMyLock mLock;

public:

	unsigned int mUID;			//编号

	unsigned int mCurrent;		//
	unsigned int mVoltage;		//
	int mTemprature;			//温度
	bool mPartake;				//是否参与
	string mName;
	

	list<CBatteryEntity> mBatteryList;


public:

	/***
	 * @brief: oprator =
	 */
	CModuleEntity& operator = (CModuleEntity& m);

	/***
	 * @brief: Add battery
	 */
	void AddBattery(CBatteryEntity be);

	/***
	 * @brief: Get battery information
	 * @param UID: 
	 */
	CBatteryEntity* GetBattery(unsigned int UID);

	/***
	 * @brief: Copy
	 */
	bool CopyFrom(CModuleEntity& m);

	void SetName(string name);
	void SetName(int name);

};

#endif