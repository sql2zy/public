#ifndef HEADER_CBRICKENTITY_SHIQINGLIANG__
#define HEADER_CBRICKENTITY_SHIQINGLIANG__

#include <iostream>
#include <entity/CBatteryEntity.h>
#include <entity/CComIDEntity.hpp>
using namespace std;

class CBrickEntity : public CComIDEntity
{
public:
	/***
	 * @brief: structure\ destructure
	 */
	CBrickEntity(void);
	~CBrickEntity(void);

public:

	unsigned char mVersion[4];		//系统版本号
	unsigned char mProductName[4];	//产品名称
	unsigned char mSoftVersion[4];	//软件版本号
	unsigned char mSOC;

	unsigned int mSettingCurrent;	//设置的电流
	unsigned int mSettingVoltage;	//设置的电压

	unsigned int mRealtimeCurrent;	//实时电流
	unsigned int mRealtimeVoltage;	//实时电压

	bool mHost;						//是否是主机
	bool mEnableState;				//状态
	char mWorkMode;					//工作状态

	char mBrickState;				//brick状态
	char mErrorCode;				//错误

	unsigned char mMinUnit;			//最小放电单元数
	char	mOutPut;				//数据上传模式

	unsigned int mDuration;			//电池单元放电时的续  航时间、充电时的充满时间
	unsigned char mTemprature;

	list<CModuleEntity> mModuleList;

public:

	/***
	 * @brief: initialize
	 */
	void Zero();

	/***
	 * @brief: operator =
	 */
	CBrickEntity& operator = (CBrickEntity& b);

	/***
	 * @breif:brick状态
	 */
	bool SetState(char state);
	char GetState(){
		return mBrickState;
	}

	/***
	 * @brief: 设置错误信息
	 */
	bool SetErrorCode(char code);

	/***
	 * @brief: 获取模块
	 */
	CModuleEntity* GetModule(unsigned int UID);

	/***
	 * @brief: 获取电池
	 */ 
	CBatteryEntity* GetBattery(unsigned int modID, unsigned int batID);

	/***
	 * @brief: Add module
	 */
	bool AddModule(CModuleEntity& me, bool replace = true);

	/***
	 * @brief: 添加
	 */
	bool AddBattery(unsigned int modID, CBatteryEntity& be, bool replase = true);

	//////////////////////////////////////////////////////////////////////////
	//模块
	/***
	 * @brief: 模块使能
	 */
	bool SetModuleEnable(unsigned int modID, bool enable);
	bool GetModuleEnable(unsigned int modID);

	/***
	 * @brief: 模块温度
	 */
	bool SetModuleTemprature(unsigned int modID, int temprature);

	/***
	 * @brief: 设置模块的电流电压
	 */
	bool SetModuleCurrent(unsigned int modID, unsigned int current);
	bool SetModuleVoltage(unsigned int modID, unsigned int voltage);
	

	//////////////////////////////////////////////////////////////////////////
	//电池操作

	/***
	 * @brief: 设置电池电流
	 */
	bool SetBatteryCurrent(unsigned int modID, unsigned int batID, unsigned int current);

	/***
	 * @brief: 设置电池电压
	 */
	bool SetBatteryVoltage(unsigned int modID, unsigned int batID, unsigned int voltage);

	/***
	 * @brief: 设置SOC
	 */
	bool SetBatterySOC(unsigned int modID, unsigned int batID, unsigned char soc);

	/***
	 * @brief: 设置电池状态
	 */
	bool SetBatteryState(unsigned int modID, unsigned int batID, unsigned char state);

	/***
	 * @brief: 设置电池开关
	 */
	bool SetBatterySwitch(unsigned int modID, unsigned int batID, unsigned char sw);

};

#endif