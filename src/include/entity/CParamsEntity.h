#include <wtypes.h>
#ifndef HEADER_CPARAMSENTITY_SHIQINGLIANG__
#define HEADER_CPARAMSENTITY_SHIQINGLIANG__

class CParamsentity
{
public:
	/***
	 * @brief: struture/destructure
	 */
	CParamsentity(void);
	CParamsentity(unsigned int uid, DWORD wparam, DWORD lparam);
	~CParamsentity(void);

	unsigned int mUID;
	DWORD mWparam;
	DWORD mLparam;

	/***
	 * @brief: ��ʼ��
	 */
	void Zero();

	/***
	 * @brief: ����
	 */
	CParamsentity& operator = (const CParamsentity& from);
};

#endif