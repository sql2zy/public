#ifndef HEADER_SITEENTITY_SHIQINGLIANG__
#define HEADER_SITEENTITY_SHIQINGLIANG__

#include <iostream>
#include <string>
#include <list>
#include <entity/CClusterEntity.h>
#include "google/protobuf/stubs/common.h"
#include <entity/CAddressEntity.hpp>
#include <entity/CComIDEntity.hpp>
using namespace std;


class CSiteEntity: public CComIDEntity
{
public:
	/***
	 * @brie: structrue/destructure
	 */
	CSiteEntity(void);
	~CSiteEntity(void);

	CHttpEntity mWebServer;			//Web Server
	CHttpEntity mCommandUrl;		//自动获取指令网址
	CNetAddrEntity mLocalServer;	//本地网址服务
	char mCommandType;				//0:被动接收 1:主动接收

	list<CClusterEntity> mClusterList;

	int mState;							//当前状态 0：1:外电正常  2：UPS供电  3: 负载部分断电 4: 负载全部断电
	__int64 mPowerCutTimestamp;			//停电时间 
	__int64 mPartialOutageTimestamp;	//部分断电时间
	__int64 mFullOutageTimestamp;		//全部断电时间
	__int64 mDuring;					//停电时，系统持续放电时间 (单位：minute)
	__int64 mEnergyStorage;				//当前储能量
	__int64 mPower;						//功率
	string mWarnings;					//警告信息


	/***
	 * @brief: 初始化
	 */
	void Zero();

	/***
	 * @brief: 设置网址
	 */
	void SetCommandUrl(string url, int interval = 2000);
	string GetCommandUrl(){

		return mCommandUrl.mUrl;
	}

	/***
	 * @brief:添加cluster
	 */
	bool AddCluster(CClusterEntity& cluster);
	
	/***
	 * @brief: 获取cluster
	 */
	CClusterEntity* GetCluster(unsigned int uid);

	/***
	 * @brief: 获取device
	 */
	CDeviceEntity* GetDevice(unsigned int cid, unsigned int did);

	/***
	 * @brief: 获取cluster数量
	 */
	unsigned int GetClustersCount();

	/***
	 * @brief: 获取device数量
	 */
	unsigned int GetDevicesCount();

	/***
	 * @brief: 获取birck数量
	 */ 
	unsigned int GetBricksCount();

protected:
private:
};

#endif