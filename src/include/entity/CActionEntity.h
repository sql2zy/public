#ifndef HEADER_CACTIONENTITY_SHIQINGLIANG__
#define HEADER_CACTIONENTITY_SHIQINGLIANG__

#include <iostream>
#include <string>
#include <Lock.hpp>
using namespace std;

class CCActionEntity
{
public:
	/***
	 * @brief: structure\destructure
	 */
	CCActionEntity(void);
	~CCActionEntity(void);

	unsigned char mCmd;
	unsigned int  mParameter;

	unsigned char mBuf[5];
	unsigned int  mBuf_len;

	unsigned int mWparam;
	unsigned int mLparam;
	char mData[8];

	char mState;

	/***
	 * @brief: 初始化
	 */
	void Zero();

	/*** 
	 * @brief: 锁定
	 */
	void doLock();

	/***
	 * @brief: 解锁
	 */
	void doUnlock();

	/***
	 * @brief: 设置数值
	 */ 
	bool SetParams(
		unsigned int cmd,
		unsigned int parameter,
		unsigned char *buf = NULL,
		unsigned int buf_len = 0,
		unsigned int wparam = 0,
		unsigned int lparam = 0,
		unsigned char* data = NULL,
		unsigned int data_len = 0);

	/***
	 * @brief: 复制
	 */
	CCActionEntity& operator = (const CCActionEntity& from);


protected:

	CMyLock mLock;
};

#endif