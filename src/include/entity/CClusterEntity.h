#ifndef HEADER_CLUSTERENTITY_SHIQINGLIANG__
#define HEADER_CLUSTERENTITY_SHIQINGLIANG__

#include <iostream>
#include <string>
#include <list>
#include <entity/CDeviceEntity.h>
#include <entity/CComIDEntity.hpp>
using namespace std;

class CClusterEntity: public CComIDEntity
{
public:
	/***
	 * @brief: structure/destructrue
	 */
	CClusterEntity(void);
	~CClusterEntity(void);
	
	int mVoltage;				//电压
	__int64	mPoewr;				//功率
	__int64 mEnergyStorage;		//当前储能量

	/***
	 * @brief 初始化
	 */
	void Zero();

	/***
	 * @brief: 设置名称
	 */
	void SetName(string name);
	string GetName(){
		return mName;
	}

	/***
	 * @brief: 设置描述
	 */
	void SetDescription(string description);
	string GetDescription(){
		return mDescription;
	}

	list<CDeviceEntity> mDeviceList;

	/***
	 * @brief: 添加device
	 */
	bool AddDevice(CDeviceEntity *device);

	/***
	 * @brief: 获取device
	 */ 
	CDeviceEntity* GetDevice(unsigned int uid);

	/***
	 * @brief: 获取Device数量
	 */
	unsigned int GetDevicesCount();

	/***
	 * @brief: 获取brick数量
	 */
	unsigned int GetBricksCount();


protected:

private:


};

#endif