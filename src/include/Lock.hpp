#ifndef HEADER_LOCK_SHIQINGLIANG__
#define HEADER_LOCK_SHIQINGLIANG__
#include <iostream>
#include <Common.h>

using namespace std;

class CMyLock
{
public: 

	/***
	 * @brief: 构造、析构
	 */
	CMyLock(void){
		com_pthread_mutex_init(&m_Lock);
	};
	~CMyLock(void){
		com_pthread_mutex_destroy(&m_Lock);
	};


	/***
	 * @brief: 锁定
	 * @param null:
	 * @return boolean
	 */
	bool Lock(void){
		com_pthread_mutex_lock(&m_Lock);
		
		return true;
	}
	bool do_lock(){return Lock();};


	/***
	 * @brief: 解锁
	 * @param null:
	 * @return boolean
	 */
	bool UnLock(void){
		com_pthread_mutex_unlock(&m_Lock);
		
		return true;
	}
	bool do_unlock(){return UnLock();};


public:

	com_pthread_mutex m_Lock;
};

#endif