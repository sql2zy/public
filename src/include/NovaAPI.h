
#ifndef HEADER_NOVAAPI_SHIQINGLIANG__
#define HEADER_NOVAAPI_SHIQINGLIANG__


#ifdef NOVA_EXPORTS
#define NOVA_API __declspec(dllexport)
#else
#define NOVA_API __declspec(dllimport)
#endif

#include <iostream>
#include <string>
using namespace std;

/***
 * char* @0: data
 * unsigned int @1: data len
 * DWORD @2:
 * void* @3:
 */
typedef void(*NOVA_CALLBACK)(char*, unsigned int,unsigned int , void*, string);

// 此类是从 Nova.dll 导出的
class NOVA_API CNovaInterface {
public:

	/***
	 * @brief: structure & destructure
	 */
	CNovaInterface(void);
	~CNovaInterface(void);

	/***
	 * @brief: add com client
	 * @param	portNo:
	 *			baud:
	 *			parity:
	 *			databits:
	 *			stopbits:
	 *			lpvoid:
	 */
	unsigned int Add(NOVA_CALLBACK cb,unsigned int portNo, unsigned int	baud, char	parity = 'N', unsigned int databits = 8,unsigned int stopbits = 1, unsigned int lpvoid = 0, void* att = NULL);
	unsigned int Add(NOVA_CALLBACK cb,unsigned int local_port, string local_addr = "",unsigned int lpvoid = 0, void* att = NULL);

	/***
	 * @brief: delete 
	 */
	void mDelete(unsigned int handle);

	/***
	 * @brief: 发送
	 */
	bool SendData(unsigned int handle,char* pData, unsigned int len,string sendtoip = "", unsigned int sendtoport = 0);

	//////////////////////////////////////////////////////////////////////////

	/***
	 * @brief: 获取模块信息
	 * @param	deviceid:设备ID
	 *			unitid:单元ID
	 *			moduleid:模块ID
	 * @return: true for success,false for failed 
	 */
	bool Nova_GetBM_Info(unsigned int handle,unsigned char deviceid, unsigned char unitid, unsigned char moduleid,string sendtoip = "", unsigned int sendtoport = 0);

	/***
	 * @brief: 获取模块信息
	 * @param	deviceid:设备ID
	 *			unitid:单元ID
	 *			state: 单元状态,{0x01: 放电; 0x02: IDLE; 0x03: 充电}
	 * @return: true for success,false for failed 
	 */
	bool Nova_SetBU_Runstate(unsigned int handle,unsigned char deviceid, unsigned char unitid,unsigned char state,string sendtoip = "", unsigned int sendtoport = 0);
	
	/***
	 * @brief: 设置模块运行状态
	 * @param	deviceid:设备ID
	 *			unitid:单元ID
	 *			moduleid:
	 *			state: 单元状态 {0x01:放电; 0x02:空闲;0x03:充电（按CMD_SET_BM_XXXXX_CFG预先下发的配置进行）;0x04: 均衡充电;0x05: 串充}
	 * @return: true for success,false for failed 
	 */
	bool Nova_SetBM_Runstate(unsigned int handle,unsigned char deviceid, unsigned char unitid, unsigned char moduleid, unsigned char state,string sendtoip = "", unsigned int sendtoport = 0);
	
	/***
	 * @brief: 设置模块运行状态
	 * @param	deviceid:设备ID
	 *			unitid:单元ID
	 *			moduleid:
	 *			charge_method: charging method 0x01:均衡充电，（无映射表）;0x02:全部串充，所有电池串参与串充;0x0A:按配置串充，按映射表充电，根据本命令按后面所附的映射表.0为开关断开，1为开关闭合.
	 *			size_battery:映射表长度，单位 bit（即电池数）
	 *			map_table:映射表
	 * @return: true for success,false for failed 
	 */
	bool Nova_SetBM_Charge_CFG(unsigned int handle,unsigned char deviceid, unsigned char unitid, unsigned char moduleid, unsigned char charge_method, 
		unsigned char size_battery, DWORD map_table,string sendtoip = "", unsigned int sendtoport = 0);

	//////////////////////////////////////////////////////////////////////////
};

#endif