#ifndef HEADER_CPROGRESS_SHIQINGLIANG
#define HEADER_CPROGRESS_SHIQINGLIANG

#include <common.h>

#define PRO_ASSERT(a,b,c)	if(a == b) goto c

class CSelfProcess 
{
public:

	/***
	 * 构造函数
	 */
	CSelfProcess(void);

	/***
	 * 析构函数
	 */
	~CSelfProcess(void);

	//退出标志
	bool m_ProExit;

	bool m_runing;

protected:
	
	//句柄
	HANDLE m_ProHand;

	unsigned int m_proID; 

public:

	/***
	 * @breif: 启动
	 * @param _StartAddress
	 * @return: 
	 */
	bool CpStart(unsigned (__stdcall * _StartAddress) (void *),void* Arglist,void*security = NULL, 
		unsigned int _StackSize = 0,unsigned int _InitFlag = 0, int Priority = THREAD_PRIORITY_NORMAL);

	bool CpStartEx(unsigned (__stdcall * _StartAddress) (void *),void* Arglist,void*security = NULL, 
		unsigned int _StackSize = 0,unsigned int _InitFlag = 0, int Priority = THREAD_PRIORITY_NORMAL);


	/***
	 * 停止
	 */
	void CpStop();
};

#endif //HEADER_CPROGRESS_SHIQINGLIANG