#ifndef HEADER_STD_SHIQINGLIANG
#define HEADER_STD_SHIQINGLIANG


//////////////////////////////////////////////////////////////////////////
//header
#ifdef WIN32

	#define WIN32_LEAN_AND_MEAN
	#include <Windows.h>
	#include <process.h>
#else

	#define  MAX_PATH 260

	#include <unistd.h>
	#include <fcntl.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdarg.h>
	#include <pthread.h>

#endif

#include <iostream>
#include <string>
using namespace std;

//通信方式
typedef enum __commu_type
{
	CT_NET = 0,		//网络模式
	CT_SERIALPORT	//串口模式
}commu_type_t;



//定义类型
/**/
#ifndef uint
#define uint unsigned int
#endif

#ifndef uchar
#define uchar unsigned char
#endif

#define _ZERO_(var) &(var[0]),sizeof(var)

#define _BZERO(var) memset(&(var[0]),0,sizeof(var))


#ifdef WIN32

	#pragma warning(once:4996)

	//日志输出
	#define STRINGIFY(x) #x
	#define TOSTRING(x) STRINGIFY(x)
	#define AT __FILE__ ":" TOSTRING(__LINE__)
	void com_error(const char *location, const char *msg);
	


	//读取配置文件
	#define GetConfigString(file, section, key, def, buf, len)	\
		GetPrivateProfileString(section,key,def,buf,len,file)

	#define SetConfigString(file,section,key,value)	\
		WritePrivateProfileString(section,key,value,file)

	//字符串比较
	#define strcasecmp(var1,var2) stricmp(var1,var2)

	#define com_printf printf

	//sleep
	#define com_sleep(to)	Sleep(to)

	//同步相关
	#define com_thread_t HANDLE
	#define com_pthread_mutex CRITICAL_SECTION
	#define com_pthread_cond	HANDLE
	#define com_pthread_mutex_init(mutex)	InitializeCriticalSection(mutex)
	#define com_pthread_event_init(event)	event = CreateEvent(NULL,FALSE,FALSE,NULL)
	#define com_pthread_mutex_lock(mutex)	EnterCriticalSection(mutex)
	#define com_pthread_mutex_unlock(mutex)	LeaveCriticalSection(mutex)
	#define com_pthread_event_signal(event)	SetEvent(event)
	#define com_pthread_event_wait(event)	WaitForSingleObject(event,INFINITE)
	#define com_pthread_event_destroy(event)	CloseHandle(event)
	#define com_pthread_mutex_destroy(mutex)	DeleteCriticalSection(mutex)
	
	void com_ShowError(DWORD errcode);

#else

	int GetCurrentPath(char buf[],char *pFileName);
	char *GetIniKeyString(char *title,char *key,char *filename);
	int GetIniKeyInt(char *title,char *key, char* def, char* buf, int buf_len, char *filename);

	#define GetConfigString(file, section, key, def, buf, len)	\
		GetIniKeyInt(section,key,def,buf,len,file)

	//sleep
	#define com_sleep(to)	sleep((to/1000))


	//同步相关
	#define com_thread_t pthread_t
	#define com_pthread_mutex pthread_mutex_t
	#define com_pthread_cond	pthread_cond_t
	#define com_pthread_mutex_init(mutex)	pthread_mutex_init(mutex)
	#define com_pthread_event_init(event)	pthread_cond_init(event)
	#define com_pthread_mutex_lock(mutex)	pthread_mutex_lock(mutex)
	#define com_pthread_mutex_unlock(mutex)	pthread_mutex_unlock(mutex)
	#define com_pthread_event_signal(event)	pthread_cond_signal(event)
	#define com_pthread_event_wait(event)	pthread_cond_wait(event)
	#define com_pthread_event_destroy(event)	pthread_cond_destroy(event)
	#define com_pthread_mutex_destroy(mutex)	pthread_mutex_destroy(mutex)
	
#endif


	//////////////////////////////////////////////////////////////////////////




	//////////////////////////////////////////////////////////////////////////

	template<typename T>
	T MMrandom(T start, T end)
	{
		return start+(end-start)*rand()/(RAND_MAX + 1.0);
	}

#endif //HEADER_STD_SHIQINGLIANG